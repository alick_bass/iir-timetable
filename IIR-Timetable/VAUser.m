//
//  VAUser.m
//  IIR-Timetable
//
//  Created by Алексей on 22.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAUser.h"
#import "VADataBaseController.h"
#import "VAAppDelegate.h"

@implementation VAUser

+ (NSArray *)userValues {
    static NSArray *userValues = nil;
    
    if ( userValues == nil ) {
        NSArray *titles = @[@"dep",@"course", @"group", @"lang", @"spec"];
        NSArray *names = @[@"Факультет", @"Курс", @"Група", @"Мова", @"Спеціалізація"];
        NSArray *optionals = @[@NO, @NO, @YES, @YES, @YES];
        NSArray *tables = @[@"departments", [NSNull null], [NSNull null], @"languages", @"specializations"];
        
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:[titles count]];
        
        for ( int i = 0; i < [titles count]; i++ ) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:3];
            [dict setObject:[titles objectAtIndex:i] forKey:USER_TITLE_KEY];
            [dict setObject:[names objectAtIndex:i] forKey:USER_NAME_KEY];
            [dict setObject:[optionals objectAtIndex:i] forKey:USER_ISOPTIONAL_KEY];
            [dict setObject:[tables objectAtIndex:i] forKey:USER_TABLE_KEY];
            [array addObject:dict];
        }
        
        userValues = array;
    }
    return userValues;
}

+ (NSArray *)userRequiredValues {
    NSArray *values = [self userValues];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF[%@] == %@", USER_ISOPTIONAL_KEY, @NO];
    return [values filteredArrayUsingPredicate:filter];
}

- (void)update:(int)value forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
    
    VADataBaseController *dataBaseController = [VADataBaseController timeTableBase];
    [dataBaseController fillInCacheDataBase];
}

- (int)getValueForKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return (int)[defaults integerForKey:key];
}

+ (id)currentUser {
    return [[self alloc] init];
}

- (void)checkSettings {
    NSArray *userValues = [VAUser userValues];
    
    BOOL settingsDefined = YES;
    
    for ( NSDictionary *userParam in userValues ) {
        NSNumber *isOptionalNumber = [userParam objectForKey:USER_ISOPTIONAL_KEY];
        NSString *title = [userParam objectForKey:USER_TITLE_KEY];
        BOOL isOptional = [isOptionalNumber boolValue];
        if ( !isOptional && ([self getValueForKey:title] == 0)) {
            settingsDefined = NO;
            break;
        }
    }
    
    [self showSettingView:settingsDefined];
}

- (void)showSettingView:(BOOL)settingsDefined {
    VAAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    UITabBarController *tabbarController = (UITabBarController *)appDelegate.window.rootViewController;
    if ( !settingsDefined )
        tabbarController.selectedIndex = TabBarSettings;
    
    NSArray *tabs = [[tabbarController tabBar] items];
    for ( UITabBarItem *tab in tabs ) {
        if ( settingsDefined ) {
            [tab setEnabled:YES];
        } else {
            [tab setEnabled:NO];
        }
    }
}

@end
