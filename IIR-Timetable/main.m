//
//  main.m
//  IIR-Timetable
//
//  Created by Алексей on 21.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VAAppDelegate class]));
    }
}
