//
//  VATeachersList.m
//  IIR-Timetable
//
//  Created by Алексей on 06.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VATeachersList.h"
#import "VADataBaseController.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "VAOpenTeacher.h"

@interface VATeachersList ()
- (void)resetSearch;
- (void)handleSearchForTerm:(NSString *)searchTerm;
@end

static NSString *segueIdentifier = @"OpenTeacherSegue";

@implementation VATeachersList

@synthesize searchBar = _searchBar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGRect barFrame = _searchBar.frame;
    barFrame.size.width = self.view.bounds.size.width;
    _searchBar.frame = barFrame;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ( [self.tableView respondsToSelector:@selector(setSectionIndexBackgroundColor:)] ) {
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    }
    _searchBar.delegate = self;
    
    [self prepareTeachersArray];
    [self resetSearch];
    [self.tableView reloadData];
    [self.tableView setContentOffset:CGPointMake(0.0, 44.0) animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareTeachersArray {
    VADataBaseController *dataBaseController = [VADataBaseController timeTableBase];
    NSArray *array = [[dataBaseController teachers] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    for ( NSString *teacher in array ) {
        NSString *firstLetter = [[teacher substringToIndex:1] uppercaseString];
        NSMutableArray *letterValues = [dict objectForKey:firstLetter];
        if ( !letterValues ) {
            letterValues = [NSMutableArray array];
            [dict setObject:letterValues forKey:firstLetter];
        }
        [letterValues addObject:teacher];
    }
    allTeachers = dict;
}

#pragma mark - Custom Search Methods

- (void)resetSearch {
    NSMutableDictionary *allNamesCopy = [allTeachers mutableDeepCopy];
    teachers = allNamesCopy;
    NSMutableArray *keyArray = [NSMutableArray arrayWithCapacity:[[allTeachers allKeys] count]];
    [keyArray addObject:UITableViewIndexSearch];
    [keyArray addObjectsFromArray:[[allTeachers allKeys] sortedArrayUsingSelector:@selector(compare:)]];
    keys = keyArray;
}

- (void)handleSearchForTerm:(NSString *)searchTerm {
    NSMutableArray *sectionsToRemove = [NSMutableArray array];
    [self resetSearch];
    
    for ( NSString *key in keys ) {
        NSMutableArray *array = [teachers valueForKey:key];
        NSMutableArray *toRemove = [NSMutableArray array];
        for ( NSString *name in array ) {
            if ( [name rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location  == NSNotFound) {
                [toRemove addObject:name];
            }
        }
        if ( [array count] == [toRemove count] ) {
            [sectionsToRemove addObject:key];
        }
        [array removeObjectsInArray:toRemove];
    }
    [keys removeObjectsInArray:sectionsToRemove];
    [self.tableView reloadData];
}

#pragma mark - Section Indexes

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if ( isSearching )
        return nil;
    return keys;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ( [keys count] > 0 ) ? [keys count] : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( [keys count] == 0 )
        return 0;
    NSString *key = [keys objectAtIndex:section];
    NSArray *nameSection = [teachers objectForKey:key];
    return [nameSection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TeachersListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [keys objectAtIndex:indexPath.section];
    NSArray *nameSection = [teachers objectForKey:key];
    cell.textLabel.text = [nameSection objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([keys count] == 0)
        return nil;
    NSString *key = [keys objectAtIndex:section];
    if ( key == UITableViewIndexSearch ) {
        return nil;
    }
    return key;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    isSearching = NO;
    _searchBar.text = @"";
    [tableView reloadData];
    [self.searchBar resignFirstResponder];
    return indexPath;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    NSString *key = [keys objectAtIndex:index];
    if ( key == UITableViewIndexSearch ) {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    } else {
        return index;
    }
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.identifier isEqualToString:segueIdentifier] ) {
        NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
        NSString *key = [keys objectAtIndex:selectedRowIndex.section];
        NSArray *nameSection = [teachers objectForKey:key];
        NSString *teacher = [nameSection objectAtIndex:selectedRowIndex.row];
        
        VAOpenTeacher *detailViewController = [segue destinationViewController];
        detailViewController.teacherName = teacher;
    }
}

#pragma mark - Search Bar Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *searchTerm = [searchBar text];
    [self handleSearchForTerm:searchTerm];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ( [searchText length] == 0 ) {
        [self resetSearch];
        [self.tableView reloadData];
        return;
    }
    [self handleSearchForTerm:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isSearching = NO;
    searchBar.text = @"";
    [self resetSearch];
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isSearching = YES;
    [self.tableView reloadData];
}

@end
