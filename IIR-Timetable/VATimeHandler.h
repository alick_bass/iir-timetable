//
//  VATimeHandler.h
//  IIR-Timetable
//
//  Created by Алексей on 31.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VATimeHandler : NSObject {
    NSArray *lessons;
}

+ (id)timeHandler;
+ (NSDate *)today;
- (NSDictionary *)lessonTimeStringForLesson:(int)lesson neededDate:(NSDate *)neededDate;

+ (const NSString *)titleKey;
+ (const NSString *)remainsKey;


+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSString *)learningWeekStrFromDate:(NSDate *)date;

+ (WeekDay)weekDayFromDate:(NSDate *)date;
+ (NSString *)weekDayName:(WeekDay)weekDay;

+ (LearningWeek)learningWeekFromDate:(NSDate *)date;
+ (NSString *)learningWeekStrFromWeek:(LearningWeek)learningWeek;

+ (NSDate *)dayBeforeDate:(NSDate *)date;
+ (NSDate *)dayAfterDate:(NSDate *)date;

+ (NSDate *)sessionDateFromString:(NSString *)dateStr;

@end
