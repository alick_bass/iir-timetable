//
//  VAOpenChannel.h
//  IIR-Timetable
//
//  Created by Алексей on 12.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum {
    ChannelCellTagImageView = 1,
    ChannelCellTagLabel,
} ChannelCellTags;

@interface VAOpenChannel : UITableViewController <NetworkDelegate> {
    NSArray *channelItems;
}

@end
