//
//  VADayController.m
//  IIR-Timetable
//
//  Created by Алексей on 30.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VADayController.h"
#import "VADataBaseController.h"
#import "VATimeHandler.h"

@interface VADayController ()

@end

static const NSString *lessonTypeKey = @"type";
static const NSString *lessonTitleKey = @"title";
static const NSString *lessonTeacherKey = @"teacher";
static const NSString *lessonRoomKey = @"room";

@implementation VADayController

@synthesize neededDate = _neededDate;

#pragma mark - LifeCycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style andDate:(NSDate *)date {
    self = [super initWithStyle:style];
    if ( self ) {
        _neededDate = date;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reset) name:UPDATE_TIMETABLE_NOTIFICATION object:nil];
    }
    return self;
}

+ (id)dayControllerWithDate:(NSDate *)date {
    return [[self alloc] initWithStyle:UITableViewStyleGrouped andDate:date];
}

- (void)resetToDate:(NSDate *)date {
    _neededDate = date;
    [self reset];
}

- (void)reset {
    [self prepareLessonsDictionary];
    [self reloadTableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareLessonsDictionary];
    
    
    UINib *countryNib = [UINib nibWithNibName:@"DayCell" bundle:nil];
    [self.tableView registerNib:countryNib
         forCellReuseIdentifier:@"DayCell"];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                             target:self
                                           selector:@selector(reloadTableView)
                                           userInfo:nil
                                            repeats:YES];
    timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(prepareLessonsDictionary) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data Base Methods

- (void)prepareLessonsDictionary {
    lessonsDict = nil;
    VADataBaseController *dataBaseController = [VADataBaseController timeTableBase];
    NSDictionary *dict = [dataBaseController getTimetableForWeekday:[VATimeHandler weekDayFromDate:_neededDate] week:[VATimeHandler learningWeekFromDate:_neededDate]];
    lessonsDict = dict;
}

#pragma mark - Helper

- (NSArray *)sortedKeys {
    NSArray *keys = [lessonsDict allKeys];
    return [keys sortedArrayUsingSelector:@selector(compare:)];
}

- (NSString *)typeStrFromNSNumber:(NSNumber *)typeNumber {
    int type = [typeNumber intValue];
    NSString *typeStr;
    switch (type) {
        case LessonTypeSeminar:
            typeStr = @"Семінар";
            break;
        
        case LessonTypeLection:
            typeStr = @"Лекція";
            break;
            
        default:
            typeStr = nil;
            break;
    }
    return typeStr;
}

#pragma mark - Table view data source

- (void)reloadTableView {
    [self.tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    int lessonNumber = [[[self sortedKeys] objectAtIndex:section] intValue];
    return [NSString stringWithFormat:@"%d ПАРА", lessonNumber];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[lessonsDict allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 98;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DayCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configerCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configerCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSArray *keys = [self sortedKeys];
    NSString *key = [keys objectAtIndex:indexPath.section];
    NSDictionary *lesson = [lessonsDict objectForKey:key];
    
    UILabel *typeLabel = (UILabel *)[cell viewWithTag:CellTagTypeLabel];
    typeLabel.text = [self typeStrFromNSNumber:[lesson objectForKey:lessonTypeKey]];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:CellTagTitleLabel];
    titleLabel.text = [lesson objectForKey:lessonTitleKey];
    
    UILabel *teacherLabel = (UILabel *)[cell viewWithTag:CellTagTecherLabel];
    teacherLabel.text = [lesson objectForKey:lessonTeacherKey];
    
    UILabel *roomLabel = (UILabel *)[cell viewWithTag:CellTagRoomLabel];
    roomLabel.text = [lesson objectForKey:lessonRoomKey];
    
    UILabel *timeLeftLabel = (UILabel *)[cell viewWithTag:CellTagLeftTimeLabel];
    UIView *backView = (UIView *)[cell viewWithTag:CellTagLeftTimeBackground];
    timeLeftLabel.backgroundColor = [UIColor clearColor];
    VATimeHandler *timeHandler = [VATimeHandler timeHandler];
    NSDictionary *timeLeftDict = [timeHandler lessonTimeStringForLesson:[key intValue] neededDate:_neededDate];
    if ( timeLeftDict ) {
        NSString *timeLeftStr = [timeLeftDict objectForKey:[VATimeHandler titleKey]];
        BOOL remains = [[timeLeftDict objectForKey:[VATimeHandler remainsKey]] boolValue];
        NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:backView
                                                                           attribute:NSLayoutAttributeWidth
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:timeLeftLabel
                                                                           attribute:NSLayoutAttributeWidth
                                                                          multiplier:1.0
                                                                            constant:10];
        [cell.contentView addConstraint:widthConstraint];
        
        if ( remains ) {
            backView.backgroundColor = [UIColor colorWithHexString:BLUE_COLOR_HEX];
        } else {
            backView.backgroundColor = [UIColor colorWithHexString:LIGHT_BLUE_COLOR_HEX];
        }
        backView.layer.cornerRadius = 11;
        backView.layer.masksToBounds = YES;
        timeLeftLabel.text = timeLeftStr;
    } else {
        timeLeftLabel.text = @"";
        backView.backgroundColor = [UIColor clearColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
