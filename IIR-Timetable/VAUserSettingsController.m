//
//  VAUserSettingsController.m
//  IIR-Timetable
//
//  Created by Алексей on 07.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAUserSettingsController.h"
#import "VAUser.h"
#import "VADataBaseController.h"

@interface VAUserSettingsController ()

@end

@implementation VAUserSettingsController

@synthesize settingsTitle = _settingsTitle;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareSettings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareSettings {
    VAUser *user = [VAUser currentUser];
    selectedSetting = [user getValueForKey:_settingsTitle];
    
    VADataBaseController *dataBaseController = [VADataBaseController timeTableBase];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:TABLE_SETTING_NAME_KEY ascending:YES];
    settings = [[dataBaseController settingsValueForColumn:_settingsTitle] sortedArrayUsingDescriptors:@[sort]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [settings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SettingsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *setting = [settings objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [setting objectForKey:TABLE_SETTING_NAME_KEY];
    
    cell.accessoryType = ( selectedSetting == [[setting objectForKey:_settingsTitle] intValue] ) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *setting = [settings objectAtIndex:indexPath.row];
    int newSelected = [[setting objectForKey:_settingsTitle] intValue];
    
    VAUser *user = [VAUser currentUser];
    [user update:newSelected forKey:_settingsTitle];
    [user checkSettings];
    
    selectedSetting = newSelected;
    [tableView reloadData];
}

@end
