//
//  Netwotk.h
//  VseBileti
//
//  Created by Алексей on 27.09.13.
//  Copyright (c) 2013 Алексей Дикань. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NetworkDelegate <NSObject>

@required

- (void)netwotkFinishedLoadingData:(NSData *)data;

@optional

- (void)networkFailedToLoadData;

@end

@interface Netwotk : NSObject <NSURLConnectionDataDelegate> {
    NSString *urlString;
    NSString *baseUrlStr;
    NSMutableData *responseData;
    NSURLConnection *connection;
    
    int totalFileSize;
    int recievedDataSize;
    
    id<NetworkDelegate> delegate;
}

@property (nonatomic, strong) id delegate;
@property (nonatomic, assign) BOOL showProgress;

+ (id)networkWithBaseUrl:(NSString *)baseUrl
                     url:(NSString *)url
                delegate:(id<NetworkDelegate>)netDelegate;
- (void)startConnection;
- (void)startPostConnectionWithHTTPBody:(NSData *)httpBody;
- (void)cancelNetworkConnection;

@end
