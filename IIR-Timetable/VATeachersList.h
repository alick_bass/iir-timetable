//
//  VATeachersList.h
//  IIR-Timetable
//
//  Created by Алексей on 06.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VATeachersList : UITableViewController <UISearchBarDelegate> {
    NSDictionary *allTeachers;
    NSMutableDictionary *teachers;
    NSMutableArray *keys;
    
    BOOL isSearching;
}

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
