//
//  VAAdvert.h
//  IIR-Timetable
//
//  Created by Алексей on 14.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VAAdConnection.h"

extern NSString * const adTypePortraitPhone;
extern NSString * const adTypePortraitPad;

@protocol VAAdvertDelegate <NSObject>

- (void)advertLoadedAd;
- (void)advertFailedToLoadAd;

@end

@interface VAAdvert : UIImageView <VAAdConnectionDelegate> {
    NSDictionary *currentAd;
    id<VAAdvertDelegate> delegate;
    NSTimer *timer;
}

@property (nonatomic, strong) id delegate;
@property (readonly, nonatomic, strong) NSString *adType;

+ (id)adWithOrigin:(CGPoint)origin;
+ (id)ad;
- (void)setOrigin:(CGPoint)origin;

@end
