//
//  VAAdConnection.h
//  IIR-Timetable
//
//  Created by Alick Dikan on 15.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VAAdConnectionDelegate <NSObject>

- (void)adConnectionRecievedAd:(NSDictionary *)ad;
- (void)adConnectionFailedToReceiveAd;

@end

@interface VAAdConnection : NSObject {
    id<VAAdConnectionDelegate> delegate;
}

@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) NSManagedObjectContext *context;

+ (id)adConnection;
- (void)downloadAds;

@end