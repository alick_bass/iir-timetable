//
//  VAUserSettingsController.h
//  IIR-Timetable
//
//  Created by Алексей on 07.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAUserSettingsController : UITableViewController {
    int selectedSetting;
    NSArray *settings;
}

@property (nonatomic, strong) NSString *settingsTitle;

@end
