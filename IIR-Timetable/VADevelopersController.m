//
//  VADevelopersController.m
//  IIR-Timetable
//
//  Created by Алексей on 09.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VADevelopersController.h"

@interface VADevelopersController ()

@end

static const NSString *sectionTitleKey = @"sectionTitle";
static const NSString *sectionDataKey = @"sectionData";

@implementation VADevelopersController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupDevelopersInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupDevelopersInfo {
    NSMutableArray *array = [NSMutableArray array];
    
    NSDictionary *websiteDict = @{sectionTitleKey: @"Наш сайт:", sectionDataKey: @[@"http://violented.by"]};
    
    NSDictionary *contactInfo = @{sectionTitleKey: @"Контакти", sectionDataKey: @[@"noobasco@gmail.com", @"063-437-01-09"]};
    [array addObject:websiteDict];
    [array addObject:contactInfo];
    developesInfo = array;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [developesInfo count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *sectionDict = [developesInfo objectAtIndex:section];
    return [(NSArray *)[sectionDict objectForKey:sectionDataKey] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDictionary *sectionDict = [developesInfo objectAtIndex:section];
    return [sectionDict objectForKey:sectionTitleKey];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DeveloperCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *section = [developesInfo objectAtIndex:indexPath.section];
    NSString *textFieldStr = (NSString *)[(NSArray *)[section objectForKey:sectionDataKey] objectAtIndex:indexPath.row];
    
    UITextView *textView = (UITextView *)[cell viewWithTag:DevelopesCellTagTextView];
    textView.text = textFieldStr;
}

@end
