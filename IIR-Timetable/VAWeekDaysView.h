//
//  VAWeekDaysView.h
//  IIR-Timetable
//
//  Created by Алексей on 28.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAWeekDaysViewDelegate <NSObject>

@required

- (void)selectedDate:(NSDate *)date fromTouch:(BOOL)fromTouch;

@end

@interface VAWeekDaysView : UIView {
    NSDate *selectedDate;
    NSDate *currentWeek;
    NSArray *currentWeekArray;
    BOOL emptyDraw;
    id<VAWeekDaysViewDelegate> delegate;
    
    float viewHeight;
    float circleDiameter;
    float circleLayout;
    float circleWhiteSpace;
    float datesStartX;
    float fontSize;
}

@property (nonatomic, strong) id delegate;

+ (id)weekDaysViewWithFrame:(CGRect)frame;
+ (id)weekDaysView;
- (float)viewHeight;
- (void)showNextDay;
- (void)showPreviosDay;
- (void)reset;

@end
