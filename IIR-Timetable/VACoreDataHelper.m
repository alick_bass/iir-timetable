//
//  VACoreDataHelper.m
//  IIR-Timetable
//
//  Created by Alick Dikan on 15.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VACoreDataHelper.h"
#import "VAAppDelegate.h"

@implementation VACoreDataHelper

+ (BOOL)entityHasData:(NSString *)entityName inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    [request setIncludesSubentities:NO];
    
    NSError *error;
    NSUInteger count = [context countForFetchRequest:request error:&error];
    
    if ( count == NSNotFound ) {
        NSLog(@"Enresolved error");
    }
    
    if (count < 1) {
        return NO;
    }
    return YES;
}

+ (void)deleteAllInstancesInEntity:(NSString *)entityName inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    [request setIncludesPropertyValues:NO];
    
    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ( [results count] > 0 ) {
        for ( NSManagedObject *managedObject in results ) {
            [context deleteObject:managedObject];
        }
        [self saveContext];
    }
}

+ (void)insertIntoCoreDataValues:(NSArray *)values
                   forEntityName:(NSString *)entityName
                       inContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    NSDictionary *attributes = [entity attributesByName];
    
    for ( NSDictionary *dict in values ) {
        NSManagedObject *newSubject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
        for ( NSString *attributeKey in attributes ) {
            NSAttributeDescription *attribute = [attributes objectForKey:attributeKey];
            if ( [attribute attributeType] == NSInteger16AttributeType ) {
                NSNumber *number = [NSNumber numberWithInteger:[[dict objectForKey:attributeKey] integerValue]];
                [newSubject setValue:number forKey:attributeKey];
            } else {
                [newSubject setValue:[dict objectForKey:attributeKey] forKey:attributeKey];
            }
        }
    }
    [self saveContext];
}

+ (void)saveContext {
    VAAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate saveContext];
}

+ (NSManagedObjectContext *)mainContext {
    VAAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    return appDelegate.managedObjectContext;
}

+ (NSArray *)getAllDataFromEntity:(NSString *)entityName inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    
    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    NSDictionary *attributes = [entity attributesByName];
    
    if ( results == nil ) {
        NSLog(@"Unresolved error");
        return nil;
    }
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[results count]];
    
    for ( NSManagedObject *managedObject in results ) {
        NSMutableDictionary *subject = [NSMutableDictionary dictionaryWithCapacity:[attributes count]];
        
        for ( NSString *attributeKey in attributes ) {
            [subject setObject:[managedObject valueForKey:attributeKey] forKey:attributeKey];
        }
        [array addObject:subject];
    }
    
    return array;
}

@end
