//
//  VAFeedback.m
//  IIR-Timetable
//
//  Created by Алексей on 13.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAFeedback.h"
#import "VAUser.h"

@interface VAFeedback ()

@end

static const NSString *messageKey = @"message";

@implementation VAFeedback

@synthesize messageView = _messageView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _messageView.delegate = self;
    
    if ( SYSTEM_VERSION_LESS_THAN(@"7.0") ) {
        _messageView.layer.cornerRadius = 7;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - VAUSER

- (NSDictionary *)userDictionary {
    NSMutableDictionary *userDict = [NSMutableDictionary dictionary];
    
    NSArray *userValues = [VAUser userValues];
    
    VAUser *user = [VAUser currentUser];
    
    for ( NSDictionary *userValue in userValues ) {
        NSString *key = [userValue objectForKey:USER_TITLE_KEY];
        int intValue = [user getValueForKey:key];
        [userDict setObject:[NSNumber numberWithInt:intValue] forKey:key];
    }
    
    return userDict;
}

#pragma mark - Actions

- (IBAction)sendFeedback:(id)sender {
    if ( [_messageView.text isEqualToString:@""] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Нема повідомлення"
                                                        message:@"Ви намагаєтесь відправити пусте повідомлення"
                                                       delegate:self
                                              cancelButtonTitle:@"Більше не буду"
                                              otherButtonTitles: nil];
        [alert show];
    } else {
        NSMutableDictionary *jsonDict = [NSMutableDictionary dictionaryWithDictionary:[self userDictionary]];
        [jsonDict setObject:_messageView.text forKey:messageKey];
        
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                           options:0 error:&error];
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSData *httpBody = [[NSString stringWithFormat:@"json=%@", jsonStr] dataUsingEncoding:NSUTF8StringEncoding];
        
        Netwotk *network = [Netwotk networkWithBaseUrl:nil
                                                   url:@"http://avtokursi.com/iOS/post.php"
                                              delegate:self];
        [network startPostConnectionWithHTTPBody:httpBody];
    }
}

#pragma mark - Network Delegate

- (void)netwotkFinishedLoadingData:(NSData *)data {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Відправлено"
                                                    message:@"Ваше повідомлення відправлено!"
                                                   delegate:self
                                          cancelButtonTitle:@"Оце так!"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - TextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ( [text isEqualToString:@"\n"] ) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - Alert View

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
