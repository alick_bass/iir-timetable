//
//  VATimeHandler.m
//  IIR-Timetable
//
//  Created by Алексей on 31.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VATimeHandler.h"
#import "VADataBaseController.h"

static const NSString *startKey = @"start";
static const NSString *endKey = @"end";

static const NSString *titleKey = @"title";
static const NSString *remainsKey = @"remains";

static NSString *sessionDateFormat = @"HH:mm dd.MM.yyyy";

@implementation VATimeHandler

- (id)init {
    self = [super init];
    
    if ( self ) {
        NSDictionary *firstPair = @{startKey: LESSON_ONE_START, endKey: LESSON_ONE_END };
        NSDictionary *secondPair = @{startKey: LESSON_TWO_START, endKey:LESSON_TWO_END};
        NSDictionary *thirdPair = @{startKey: LESSON_THREE_START, endKey:LESSON_THREE_END};
        NSDictionary *fourthPair = @{startKey: LESSON_FOUR_START, endKey:LESSON_FOUR_END};
        lessons = @[firstPair, secondPair, thirdPair, fourthPair];
    }
    return self;
}

+ (id)timeHandler {
    return [[self alloc] init];
}

- (NSDictionary *)lessonTimeStringForLesson:(int)lesson neededDate:(NSDate *)neededDate {
    if ( [neededDate compare:[VATimeHandler today]] == NSOrderedDescending ) {
        return nil;
    }
    
    NSDictionary *neededLesson = [lessons objectAtIndex:lesson-1];
    
    NSDate *lessonStartDate = [self neededDate:neededDate hoursAndMinutes:[neededLesson objectForKey:startKey]];
    NSDate *lessonEndDate = [self neededDate:neededDate hoursAndMinutes:[neededLesson objectForKey:endKey]];
    NSDate *now = [[NSDate date] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
    
    if ( [now compare:lessonStartDate] == NSOrderedAscending ) {
        NSString *leftTimeStr = [self timeLeftStrFromDate:now toDate:lessonStartDate];
        NSDictionary *dict = @{titleKey: [NSString stringWithFormat:@"Через: %@", leftTimeStr],
                               remainsKey: @NO};
        return dict;
    } else if ( [now compare:lessonEndDate] == NSOrderedAscending ) {
        NSString *leftTimeStr = [self timeLeftStrFromDate:now toDate:lessonEndDate];
        NSDictionary *dict = @{titleKey: [NSString stringWithFormat:@"Залишилося: %@", leftTimeStr],
                               remainsKey:@YES};
        return dict;
    }
    return nil;
}

#pragma mark - NSDateMethods

- (NSDate *)neededDate:(NSDate *)neededDate hoursAndMinutes:(NSString *)hoursAndMinutes {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *dateHM = [[dateFormatter dateFromString:hoursAndMinutes] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
    NSDateComponents *hMComp = [gregorian components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:dateHM];
    
    NSDateComponents *neededComps = [gregorian components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:neededDate];
    [neededComps setHour:hMComp.hour];
    [neededComps setMinute:hMComp.minute];
    return [gregorian dateFromComponents:neededComps];
}

+(NSDate *)today {
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components: (NSYearCalendarUnit | NSMonthCalendarUnit |NSDayCalendarUnit) fromDate:now];
    return [[gregorian dateFromComponents:components] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
}

#pragma mark - String Methods

- (NSString *)timeLeftStrFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger components = NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *difference = [gregorian components:components fromDate:fromDate toDate:toDate options:0];
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)[difference hour], (long)[difference minute]];
}

#pragma mark - Static Getters

+ (const NSString *)titleKey {
    return titleKey;
}

+ (const NSString *)remainsKey {
    return remainsKey;
}

#pragma mark - Static Methods

+ (NSString *)stringFromDate:(NSDate *)date {
    NSArray *months = @[@"січня",@"лютого",@"березня",
                        @"квітня",@"травня",@"червня",
                        @"липня",@"серпня",@"вересня",
                        @"жовтня",@"листопада",@"грудня"];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components: (NSYearCalendarUnit | NSMonthCalendarUnit |NSDayCalendarUnit) fromDate:date];
    return [NSString stringWithFormat:@"%ld %@ %ld", (long)components.day, [months objectAtIndex:components.month-1], (long)components.year];
}

+ (NSString *)learningWeekStrFromDate:(NSDate *)date {
    LearningWeek week = [self learningWeekFromDate:date];
    if ( week == LearningWeekHigh ) {
        return @"верхній тиждень";
    }
    if ( week == LearningWeekLow ) {
        return @"нижній тиждень";
    }
    return nil;
}

+ (NSString *)learningWeekStrFromWeek:(LearningWeek)learningWeek {
    if ( learningWeek == LearningWeekHigh ) {
        return @"верхній тиждень";
    }
    if ( learningWeek == LearningWeekLow ) {
        return @"нижній тиждень";
    }
    return nil;
}

+ (WeekDay)weekDayFromDate:(NSDate *)date {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    WeekDay weekday = (int)comps.weekday;
    return weekday;
}

+ (NSString *)weekDayName:(WeekDay)weekDay {
    NSArray *weekDays = @[@"Неділя", @"Понеділок", @"Вівторок", @"Середа", @"Четвер", @"П'ятниця", @"Субота"];
    return [weekDays objectAtIndex:weekDay-1];
}

+ (LearningWeek)learningWeekFromDate:(NSDate *)date {
    VADataBaseController *dataBaseController = [VADataBaseController timeTableBase];
    int baseWeek = [dataBaseController getLearningWeek];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setMinimumDaysInFirstWeek:7];
    NSDateComponents *comps = [gregorian components:NSWeekCalendarUnit fromDate:date];
    
    int learningWeek = 0;
    if (comps.week%2 == 0) {
        if (baseWeek == 1)
            learningWeek = LearningWeekHigh;
        else if (baseWeek == 2)
            learningWeek = LearningWeekLow;
    }
    else {
        if (baseWeek == 1)
            learningWeek = LearningWeekLow;
        else if ( baseWeek == 2 )
            learningWeek = LearningWeekHigh;
    }
    return learningWeek;
}

+ (NSDate *)dayBeforeDate:(NSDate *)date {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compsToAdd = [[NSDateComponents alloc] init];
    [compsToAdd setDay:-1];
    return [gregorian dateByAddingComponents:compsToAdd
                               toDate:date
                              options:0];
}

+ (NSDate *)dayAfterDate:(NSDate *)date {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compsToAdd = [[NSDateComponents alloc] init];
    [compsToAdd setDay:1];
    return [gregorian dateByAddingComponents:compsToAdd
                                      toDate:date
                                     options:0];
}

+ (NSDate *)sessionDateFromString:(NSString *)dateStr {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:sessionDateFormat];
    return [dateFormatter dateFromString:dateStr];
}

@end
