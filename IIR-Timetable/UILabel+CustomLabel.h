//
//  UILabel+CustomLabel.h
//  IIR-Timetable
//
//  Created by Алексей on 28.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (CustomLabel)

+ (id)labelWithFrame:(CGRect)frame
            fontSize:(float)fontSize
           fontColor:(UIColor *)fontColor;

+ (id)labelWithFontSize:(float)fontSize
              fontColor:(UIColor *)fontColor;

@end
