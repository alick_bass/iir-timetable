//
//  VAMainDayController.m
//  IIR-Timetable
//
//  Created by Alick Dikan on 21.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAMainDayController.h"
#import "VADayController.h"
#import "VATimeHandler.h"
#import "UIColor+expanded.h"

@interface VAMainDayController ()

@end

@implementation VAMainDayController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|
     UIViewAutoresizingFlexibleHeight |
     UIViewAutoresizingFlexibleBottomMargin |
     UIViewAutoresizingFlexibleRightMargin];
    self.view.backgroundColor = [UIColor colorWithHexString:NAVBAR_HEXCOLOR];
    [super viewDidLoad];
    [self setupViews];
    [self setupNavBar];
    [self setupScrollContentViews];
}

- (void)viewWillAppear:(BOOL)animated {
    tempContentOffset = _mainScroll.contentOffset;
    _mainScroll.contentOffset = CGPointZero;
}

- (void)viewDidAppear:(BOOL)animated {
    _mainScroll.contentOffset = tempContentOffset;
    
    CGRect selfBounds = self.view.bounds;
    CGFloat scrollWidth = selfBounds.size.width;
    
    [_mainScroll scrollRectToVisible:CGRectMake(0.0, 0.0, scrollWidth, _mainScroll.frame.size.height) animated:NO];
    [_mainScroll scrollRectToVisible:CGRectMake(scrollWidth, 0.0, scrollWidth, _mainScroll.frame.size.height) animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Views Setup Methods

- (void)setupViews {
    _mainScroll = [[UIScrollView alloc] init];
    _mainScroll.translatesAutoresizingMaskIntoConstraints = NO;
    _mainScroll.pagingEnabled = YES;
    _mainScroll.delegate = self;
    _mainScroll.bounces = NO;
    _mainScroll.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_mainScroll];
    
    _adView = [VAAdvert ad];
    _adView.delegate = self;
    _adView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_adView];
    
    _navBar = [[UIView alloc] init];
    _navBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_navBar];
    
    CGSize adSize = CGSizeFromString(_adView.adType);
    NSDictionary *metrics = @{@"adWidth": @(adSize.width),
                              @"adHeight": @(adSize.height)};
    
    NSDictionary *views = @{@"navBar": _navBar,
                            @"scrollView": _mainScroll,
                            @"adView": _adView};
    
    navBarOffsetConstraint = [NSLayoutConstraint constraintWithItem:_navBar
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0];
    [self.view addConstraint:navBarOffsetConstraint];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[navBar]|"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[navBar][scrollView]|"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[adView(adHeight)][navBar]"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_adView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:adSize.width]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_adView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:_navBar
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
}

- (void)setupNavBar {
    _navBar.backgroundColor = [UIColor colorWithHexString:NAVBAR_HEXCOLOR];
    _navBar.layer.masksToBounds = NO;
    _navBar.layer.shadowOffset = CGSizeMake(0.0, 1.0f);
    _navBar.layer.shadowOpacity = 0.6f;
    
    titleLabel = [UILabel labelWithFontSize:18 fontColor:[UIColor blackColor]];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.text = [VATimeHandler stringFromDate:[VATimeHandler today]];
    [_navBar addSubview:titleLabel];
    
    learningWeek = [UILabel labelWithFontSize:14 fontColor:[UIColor colorWithHexString:BLUE_COLOR_HEX]];
    learningWeek.translatesAutoresizingMaskIntoConstraints = NO;
    learningWeek.text = [VATimeHandler learningWeekStrFromDate:[VATimeHandler today]];
    [_navBar addSubview:learningWeek];
    
    weekDaysView = [VAWeekDaysView weekDaysView];
    weekDaysView.translatesAutoresizingMaskIntoConstraints = NO;
    weekDaysView.delegate = self;
    [_navBar addSubview:weekDaysView];
    
    NSDictionary *metrics = @{@"topOffset": @(STATUS_BAR_HEIGHT),
                              @"weekDaysHeight": @([weekDaysView viewHeight])};
    NSDictionary *views = @{@"titleLabel": titleLabel,
                            @"weekLabel": learningWeek,
                            @"weekDaysView": weekDaysView};
    
    navBarTopInsetConstraint = [NSLayoutConstraint constraintWithItem:titleLabel
                                                            attribute:NSLayoutAttributeTop
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:_navBar
                                                            attribute:NSLayoutAttributeTop
                                                           multiplier:1.0
                                                             constant:STATUS_BAR_HEIGHT];
    [_navBar addConstraint:navBarTopInsetConstraint];
    
    [_navBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[titleLabel]|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views]];
    [_navBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[weekLabel]|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views]];
    [_navBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[weekDaysView]|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views]];
    [_navBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel][weekLabel][weekDaysView(weekDaysHeight)]|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views]];
}

- (void)setupScrollContentViews {
    
    NSDate *today = [VATimeHandler today];
    
    leftDayView = [VADayController dayControllerWithDate:[VATimeHandler dayBeforeDate:today]];
    leftDayView.view.translatesAutoresizingMaskIntoConstraints = NO;
    [_mainScroll addSubview:leftDayView.view];
    
    selectedDayView = [VADayController dayControllerWithDate:today];
    selectedDayView.view.translatesAutoresizingMaskIntoConstraints = NO;
    [_mainScroll addSubview:selectedDayView.view];
    
    rightDayView = [VADayController dayControllerWithDate:[VATimeHandler dayAfterDate:today]];
    rightDayView.view.translatesAutoresizingMaskIntoConstraints = NO;
    [_mainScroll addSubview:rightDayView.view];
    
    [self layoutDayControllers];
}

- (void)layoutDayControllers {
    NSDictionary *scrollMetrics = @{@"tabbarHeight": @(TABBAR_HEIGHT)};
    
    NSDictionary *scrollContentViews = @{@"leftView": leftDayView.view,
                                         @"centerView": selectedDayView.view,
                                         @"rightView": rightDayView.view,
                                         @"scrollView": _mainScroll};
    
    [_mainScroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[leftView(==scrollView)][centerView(==scrollView)][rightView(==scrollView)]|"
                                                                        options:0
                                                                        metrics:scrollMetrics
                                                                          views:scrollContentViews]];
    [_mainScroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftView(==scrollView)]|"
                                                                        options:0
                                                                        metrics:scrollMetrics
                                                                          views:scrollContentViews]];
    [_mainScroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[centerView(==scrollView)]|"
                                                                        options:0
                                                                        metrics:scrollMetrics
                                                                          views:scrollContentViews]];
    [_mainScroll addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightView(==scrollView)]|"
                                                                        options:0
                                                                        metrics:scrollMetrics
                                                                          views:scrollContentViews]];
}

#pragma mark - Day Controllers Methods

- (void)resetDayControllersWithMiddleDate:(NSDate *)date {
    [leftDayView resetToDate:[VATimeHandler dayBeforeDate:date]];
    [selectedDayView resetToDate:date];
    [rightDayView resetToDate:[VATimeHandler dayAfterDate:date]];
}

#pragma mark - VAWeekDaysViewDelegate

- (void)selectedDate:(NSDate *)date fromTouch:(BOOL)fromTouch {
    titleLabel.text = [VATimeHandler stringFromDate:date];
    learningWeek.text = [VATimeHandler learningWeekStrFromDate:date];
    if ( fromTouch ) {
        [self moveScrollViewToDate:date];
    }
}

#pragma mark - Scroll View Delegate Methods

- (void)moveScrollViewToDate:(NSDate *)date {
    NSDate *selectedDate = selectedDayView.neededDate;
    NSComparisonResult dateComparison = [selectedDate compare:date];
    if ( dateComparison == NSOrderedSame ) {
        return;
    }
    float scrollHeight = _mainScroll.frame.size.height;
    float scrollWidth = _mainScroll.frame.size.width;
    
    if ( dateComparison == NSOrderedAscending ) {
        [rightDayView resetToDate:date];
        [_mainScroll scrollRectToVisible:CGRectMake(scrollWidth * 2, 0.0, scrollWidth, scrollHeight) animated:YES];
    } else {
        [leftDayView resetToDate:date];
        [_mainScroll scrollRectToVisible:CGRectMake(0, 0, scrollWidth, scrollHeight) animated:YES];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float scrollHeight = _mainScroll.frame.size.height;
    float scrollWidth = _mainScroll.frame.size.width;
    
    if(scrollView.contentOffset.x > scrollView.frame.size.width) {
        [self resetDayControllersWithMiddleDate:rightDayView.neededDate];
        [weekDaysView showNextDay];
        [selectedDayView.tableView scrollRectToVisible:CGRectMake(0, 0, scrollWidth, 1) animated:NO];
    }
    if(scrollView.contentOffset.x < scrollView.frame.size.width) {
        [self resetDayControllersWithMiddleDate:leftDayView.neededDate];
        [weekDaysView showPreviosDay];
        [selectedDayView.tableView scrollRectToVisible:CGRectMake(0, 0, scrollWidth, 1) animated:NO];
    }
    [scrollView scrollRectToVisible:CGRectMake(scrollWidth, 0.0, scrollWidth, scrollHeight) animated:NO];
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    float scrollHeight = _mainScroll.frame.size.height;
    float scrollWidth = _mainScroll.frame.size.width;
    
    if ( scrollView.contentOffset.x > scrollView.frame.size.width ) {
        [self resetDayControllersWithMiddleDate:rightDayView.neededDate];
    }
    if ( scrollView.contentOffset.x < scrollView.frame.size.width ) {
        [self resetDayControllersWithMiddleDate:leftDayView.neededDate];
    }
    
    [scrollView scrollRectToVisible:CGRectMake(scrollWidth, 0.0, scrollWidth, scrollHeight) animated:NO];
    [selectedDayView.tableView scrollRectToVisible:CGRectMake(0, 0, scrollWidth, 1) animated:NO];
}

#pragma mark - Auto Layout Methods

- (void)viewWillLayoutSubviews {
    [weekDaysView setNeedsDisplay];
}

- (void)viewDidLayoutSubviews {
    CGRect selfBounds = self.view.bounds;
    
    CGFloat scrollWidth = selfBounds.size.width;
    
    [_mainScroll scrollRectToVisible:CGRectMake(scrollWidth, 0.0, scrollWidth, _mainScroll.frame.size.height) animated:NO];
}

#pragma mark - VAAdvert

- (void)advertLoadedAd {
    if ( navBarOffsetConstraint.constant == 0.0 ) {
        [self animateAdvert];
    }
}

- (void)advertFailedToLoadAd {
    if ( navBarOffsetConstraint.constant > 0 ) {
        [self animateAdvert];
    }
}

- (void)animateAdvert {
    CGSize adSize = CGSizeFromString(_adView.adType);
    adSize.height += STATUS_BAR_HEIGHT;
    
    if ( navBarOffsetConstraint.constant > 0 ) {
        navBarOffsetConstraint.constant = 0.0f;
        navBarTopInsetConstraint.constant = STATUS_BAR_HEIGHT;
    } else {
        navBarOffsetConstraint.constant = adSize.height;
        navBarTopInsetConstraint.constant = 0.0f;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
    }];
}

@end
