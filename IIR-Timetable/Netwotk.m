//
//  Netwotk.m
//  VseBileti
//
//  Created by Алексей on 27.09.13.
//  Copyright (c) 2013 Алексей Дикань. All rights reserved.
//

#import "Netwotk.h"
#import "SVProgressHUD.h"

@implementation Netwotk

static NSString *networkStatus = @"Завантаження";

@synthesize delegate;
@synthesize showProgress=_showProgress;

+ (id)networkWithBaseUrl:(NSString *)baseUrl
                     url:(NSString *)url
                delegate:(id<NetworkDelegate>)netDelegate {
    return [[self alloc] initWithBaseUrl:baseUrl
                                     url:url
                                delegate:netDelegate];
}

- (id)initWithBaseUrl:(NSString *)baseUrl url:(NSString *)url delegate:(id<NetworkDelegate>)netDelegate {
    self = [super init];
    if ( self ) {
        baseUrlStr = baseUrl;
        urlString = url;
        self.delegate = netDelegate;
        _showProgress = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelNetworkConnection) name:CANCEL_NETWOTK_NOTIFICATION_NAME object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Networking

- (void)startConnection {
    NSURL *connectionUrl = [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:baseUrlStr]];
    NSURLRequest *request = [NSURLRequest requestWithURL:connectionUrl];
    [self showProgressView];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}

- (void)startPostConnectionWithHTTPBody:(NSData *)httpBody {
    NSURL *connectionUrl = [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:baseUrlStr]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:connectionUrl];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:httpBody];
    
    [self showProgressView];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}

- (void)cancelNetworkConnection {
    [SVProgressHUD dismiss];
    [connection cancel];
}

#pragma mark - SVProgressHud

- (void)showProgressView {
    if ( !_showProgress ) {
        [SVProgressHUD show];
    } else {
        [SVProgressHUD showProgress:0.0 status:networkStatus maskType:SVProgressHUDMaskTypeGradient];
    }
}

#pragma mark - NSURLConnection Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [NSMutableData data];
    if ( _showProgress ) {
        totalFileSize = (int)[response expectedContentLength];
        recievedDataSize = 0;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
    if ( _showProgress ) {
        recievedDataSize += [data length];
        float loaded = (float)recievedDataSize / (float)totalFileSize;
        [SVProgressHUD showProgress:loaded status:networkStatus maskType:SVProgressHUDMaskTypeGradient];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [SVProgressHUD dismiss];
    if ( [delegate respondsToSelector:@selector(netwotkFinishedLoadingData:)] )
        [delegate netwotkFinishedLoadingData:responseData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Упс"
                                                    message:@"Щось трапилось з інетіком"
                                                   delegate:nil
                                          cancelButtonTitle:@"оукей"
                                          otherButtonTitles: nil];
    [alert show];
    
    if ( [delegate respondsToSelector:@selector(networkFailedToLoadData)] ) {
        [delegate networkFailedToLoadData];
    }
}

@end