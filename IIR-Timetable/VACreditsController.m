//
//  VACreditsController.m
//  IIR-Timetable
//
//  Created by Алексей on 08.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VACreditsController.h"
#import "VADataBaseController.h"
#import "VATimeHandler.h"

@interface VACreditsController ()

@end

const static NSString *dateKey = @"date";
const static NSString *roomKey = @"room";
const static NSString *teacherKey = @"teacher";
const static NSString *titleKey = @"title";

@implementation VACreditsController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareCredits];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareCredits) name:UPDATE_TIMETABLE_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareCredits {
    VADataBaseController *databaseController = [VADataBaseController timeTableBase];
    NSArray *array = [databaseController getCredits];
    array = [array sortedArrayUsingComparator:^(id a, id b){
        NSDictionary *dictA = (NSDictionary *)a;
        NSDictionary *dictB = (NSDictionary *)b;
        
        NSString *strA = [dictA objectForKey:dateKey];
        NSString *strB = [dictB objectForKey:dateKey];
        
        NSDate *dateA = [VATimeHandler sessionDateFromString:strA];
        NSDate *dateB = [VATimeHandler sessionDateFromString:strB];
        
        return [dateA compare:dateB];
    }];
    credits = array;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [credits count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CreditsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *credit = [credits objectAtIndex:indexPath.section];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:CreditsCellTagTitle];
    titleLabel.text = [credit objectForKey:titleKey];
    
    UILabel *teacherLabel = (UILabel *)[cell viewWithTag:CreditsCellTagTeacher];
    teacherLabel.text = [credit objectForKey:teacherKey];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:CreditsCellTagDate];
    dateLabel.text = [credit objectForKey:dateKey];
    
    UILabel *roomLabel = (UILabel *)[cell viewWithTag:CreditsCellTagRoom];
    roomLabel.text = [credit objectForKey:roomKey];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"%d залік", (int)section + 1];
}

@end
