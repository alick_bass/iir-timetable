//
//  VAOpenChannel.m
//  IIR-Timetable
//
//  Created by Алексей on 12.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAOpenChannel.h"
#import "VAYoutubePlaylist.h"

@interface VAOpenChannel ()

@end

static const NSString *openChannelUrl = @"playlists";

static const NSString *partKey = @"part";
static const NSString *channelIdKey = @"channelId";
static const NSString *apiKeyKey = @"key";

static const NSString *titleKey = @"title";
static const NSString *itemsKey = @"items";
static const NSString *imageKey = @"image";
static const NSString *snippetKey = @"snippet";
static const NSString *thumbnailsKey = @"thumbnails";
static const NSString *defaultImageKey = @"high";
static const NSString *imgUrlKey = @"url";
static const NSString *idKey = @"id";

static NSString *segueIdentifier = @"OpenPlaylistItem";

@implementation VAOpenChannel

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(getChannelInfo) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self getChannelInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getChannelInfo {
    NSString *urlStr = [NSString stringWithFormat:@"%@?%@=snippet&%@=%@&%@=%@", openChannelUrl, partKey, channelIdKey, YOUTUBE_CHANNELID, apiKeyKey, YOUTUBE_API_KEY];
    Netwotk *network = [Netwotk networkWithBaseUrl:YOUTUBE_BASE_HREF
                                               url:urlStr
                                          delegate:self];
    [network startConnection];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [channelItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChannelCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableDictionary *channelItem = [channelItems objectAtIndex:indexPath.row];
    
    UIImageView *channelItemImageView = (UIImageView *)[cell viewWithTag:ChannelCellTagImageView];
    channelItemImageView.layer.cornerRadius = channelItemImageView.frame.size.height / 2;
    channelItemImageView.clipsToBounds = YES;
    
    if ( [channelItem objectForKey:imageKey] ) {
        channelItemImageView.alpha = 0;
        channelItemImageView.image = [channelItem objectForKey:imageKey];
        [UIView animateWithDuration:0.5 animations:^{
            channelItemImageView.alpha = 1;
        }];
    } else {
        channelItemImageView.image = nil;
        NSString *imgUrl = [[[[channelItem objectForKey:snippetKey] objectForKey:thumbnailsKey] objectForKey:defaultImageKey] objectForKey:imgUrlKey];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            UIImage *image = [UIImage cachedImageWithUrl:imgUrl relativeUrl:nil];
            UIImage *squreImage = [image squareImage];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [channelItem setObject:squreImage forKey:imageKey];
                if ( ([self.tableView numberOfRowsInSection:indexPath.section] > indexPath.row) && self.view.window )
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            });
        });
    }
    
    UILabel *textLabel = (UILabel *)[cell viewWithTag:ChannelCellTagLabel];
    textLabel.text = [[channelItem objectForKey:snippetKey] objectForKey:titleKey];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:segueIdentifier] ) {
        NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
        NSDictionary *channelItem = [channelItems objectAtIndex:selectedRowIndex.row];
        VAYoutubePlaylist *destinationController = [segue destinationViewController];
        destinationController.playlistId = [channelItem objectForKey:idKey];
        destinationController.title = [[channelItem objectForKey:snippetKey] objectForKey:titleKey];
    }
}

#pragma mark - Network Delegate

- (void)netwotkFinishedLoadingData:(NSData *)data {
    NSError *error = nil;
    
    id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSAssert([result isKindOfClass:[NSDictionary class]], @"Not a dictionary");
    NSDictionary *resultDict = (NSDictionary *)result;
    NSArray *resultArray = [resultDict objectForKey:itemsKey];
    channelItems = resultArray;
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void)networkFailedToLoadData {
    [self.refreshControl endRefreshing];
}

@end
