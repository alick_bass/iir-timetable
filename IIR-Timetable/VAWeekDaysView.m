//
//  VAWeekDaysView.m
//  IIR-Timetable
//
//  Created by Алексей on 28.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAWeekDaysView.h"
#import "VATimeHandler.h"

static const float viewHeightPhone = 45.0f;
static const float viewHeightPad = 50.0f;

static const float circleDiameterPhone = 35.0f;
static const float circleDiameterPad = 40.0f;

static const float circleY = 5.0f;
static const float datesStartXPhone = 23.0f;
static const float datesStartXPad = 100.0f;

static const float fontSizePhone = 17.0f;
static const float fontSizePad = 18.0f;

@implementation VAWeekDaysView

@synthesize delegate;

#pragma mark - Public Methods

+ (id)weekDaysView {
    return [[self alloc] initWithFrame:CGRectZero];
}

+ (id)weekDaysViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y,
                                                  frame.size.width, frame.size.height)];
}

- (void)showNextDay {
    [self shiftDay:1];
    if ([delegate respondsToSelector:@selector(selectedDate:fromTouch:)]) {
        [delegate selectedDate:selectedDate fromTouch:NO];
    }
}

- (void)showPreviosDay {
    [self shiftDay:-1];
    if ([delegate respondsToSelector:@selector(selectedDate:fromTouch:)]) {
        [delegate selectedDate:selectedDate fromTouch:NO];
    }
}

- (void)reset {
    self.backgroundColor = [UIColor clearColor];
    selectedDate = [VATimeHandler today];
    currentWeek = [VATimeHandler today];
    [self prepareCurrentWeekArrays];
    if ([delegate respondsToSelector:@selector(selectedDate:fromTouch:)]) {
        [delegate selectedDate:selectedDate fromTouch:YES];
    }
    [self setNeedsDisplay];
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self updateFigures];
        self.backgroundColor = [UIColor clearColor];
        selectedDate = [VATimeHandler today];
        currentWeek = [VATimeHandler today];
        [self prepareCurrentWeekArrays];
    }
    return self;
}

#pragma mark - Public Getters

- (float)viewHeight {
    return viewHeight;
}

#pragma mark - View Frames Calc

- (void)updateFigures {
    if ( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ) {
        viewHeight = viewHeightPhone;
        circleDiameter = circleDiameterPhone;
        datesStartX = datesStartXPhone;
        fontSize = fontSizePhone;
    } else {
        viewHeight = viewHeightPad;
        circleDiameter = circleDiameterPad;
        datesStartX = datesStartXPad;
        fontSize = fontSizePad;
    }
    float circlesViewWidth = self.frame.size.width - datesStartX * 2;
    circleWhiteSpace = lroundf((circlesViewWidth - (circleDiameter * 7))/6);
    
    circleLayout = circleDiameter + circleWhiteSpace;
}

#pragma mark - Next/Previous Week

- (void)moveToNextWeek {
    emptyDraw = NO;
    UIImage *currentWeekImage = [self currentWeekImage];
    [self shiftWeekForward:YES];
    selectedDate = [currentWeekArray objectAtIndex:0];
    
    [self setNeedsDisplay];
    
    UIImage *nextWeekImage = [self currentWeekImage];
    
    emptyDraw = YES;
    [self setNeedsDisplay];
    
    UIView *animationHolder = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, viewHeight)];
    [animationHolder setClipsToBounds:YES];
    [self addSubview:animationHolder];
    
    UIImageView *leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, viewHeight)];
    leftImageView.image = currentWeekImage;
    UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0.0, self.frame.size.width, viewHeight)];
    rightImageView.image = nextWeekImage;
    [animationHolder addSubview:leftImageView];
    [animationHolder addSubview:rightImageView];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         leftImageView.frame = CGRectMake(-self.frame.size.width,
                                                          0.0,
                                                          leftImageView.frame.size.width,
                                                          leftImageView.frame.size.height);
                         rightImageView.frame = CGRectMake(0.0,
                                                           0.0,
                                                           rightImageView.frame.size.width,
                                                           rightImageView.frame.size.height);
                     } completion:^(BOOL finished){
                         emptyDraw = NO;
                         [self setNeedsDisplay];
                         [animationHolder removeFromSuperview];
                     }];
}

- (void)moveToPreviousWeek {
    emptyDraw = NO;
    UIImage *currentWeekImage = [self currentWeekImage];
    [self shiftWeekForward:NO];
    selectedDate = [currentWeekArray lastObject];
    
    [self setNeedsDisplay];
    
    UIImage *previousWeekImage = [self currentWeekImage];
    
    emptyDraw = YES;
    [self setNeedsDisplay];
    
    UIView *animationHolder = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, viewHeight)];
    [animationHolder setClipsToBounds:YES];
    [self addSubview:animationHolder];
    
    UIImageView *leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-self.frame.size.width, 0.0, self.frame.size.width, viewHeight)];
    leftImageView.image = previousWeekImage;
    UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, viewHeight)];
    rightImageView.image = currentWeekImage;
    [animationHolder addSubview:leftImageView];
    [animationHolder addSubview:rightImageView];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         leftImageView.frame = CGRectMake(0.0,
                                                          0.0,
                                                          leftImageView.frame.size.width,
                                                          leftImageView.frame.size.height);
                         rightImageView.frame = CGRectMake(self.frame.size.width,
                                                           0.0,
                                                           rightImageView.frame.size.width,
                                                           rightImageView.frame.size.height);
                     } completion:^(BOOL finished){
                         emptyDraw = NO;
                         [self setNeedsDisplay];
                         [animationHolder removeFromSuperview];
                     }];
}

#pragma NSDate Methods

- (void)shiftWeekForward:(BOOL)forward {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compsToSubtract = [[NSDateComponents alloc] init];
    if ( forward ) {
        [compsToSubtract setDay:7];
    } else {
        [compsToSubtract setDay:-7];
    }
    currentWeek = [gregorian dateByAddingComponents:compsToSubtract
                                             toDate:currentWeek
                                            options:0];
    [self prepareCurrentWeekArrays];
}

- (void)shiftDay:(int)day {
    int selectedDayIndex = [self selectedDateIndex];
    
    if ( selectedDayIndex + day < 0 ) {
        [self moveToPreviousWeek];
    } else if ( selectedDayIndex + day >= [currentWeekArray count] ) {
        [self moveToNextWeek];
    } else {
        selectedDayIndex += day;
        selectedDate = [currentWeekArray objectAtIndex:selectedDayIndex];
        [self animateCircles];
    }
}

- (void)prepareCurrentWeekArrays {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComps = [gregorian components: NSWeekdayCalendarUnit fromDate:currentWeek];
    NSDateComponents *compsToSubtract = [[NSDateComponents alloc] init];
    
    NSMutableArray *dates = [NSMutableArray arrayWithCapacity:7];
    if ( weekdayComps.weekday == Sunday ) {
        for ( int i = 6; i >= 0; i-- ) {
            [compsToSubtract setDay:-i];
            NSDate *neededDate = [gregorian dateByAddingComponents:compsToSubtract
                                                            toDate:currentWeek
                                                           options:0];
            [dates addObject:neededDate];
        }
    } else {
        for ( int i = 2; i < 9; i++) {
            [compsToSubtract setDay:- (weekdayComps.weekday - i)];
            NSDate *neededDate = [gregorian dateByAddingComponents:compsToSubtract
                                                            toDate:currentWeek
                                                           options:0];
            [dates addObject:neededDate];
        }
    }
    currentWeekArray = dates;
}

- (int)selectedDateIndex {
    for ( int i = 0; i < [currentWeekArray count]; i++ ) {
        if ( [selectedDate compare:(NSDate *)[currentWeekArray objectAtIndex:i]] == NSOrderedSame ) {
            return i;
        }
    }
    return -1;
}

#pragma mark - Custom Drawing

- (UIImage *)currentWeekImage {
    UIGraphicsBeginImageContext(CGSizeMake(self.frame.size.width, viewHeight));
    CGContextRef c = UIGraphicsGetCurrentContext();
    [self.layer renderInContext:c];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

- (void)drawRect:(CGRect)rect {
    if ( !emptyDraw ) {
        [self updateFigures];
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        for ( int i = 0, x = datesStartX; i < [currentWeekArray count]; i++, x += circleLayout ) {
            if ( [self selectedDateIndex] == i ) {
                [self drawCircleWithOrigin:CGPointMake(x, circleY) inContext:context];
            }
        }
        [self drawWeekDaysNamesInContext:context];
    }
}

- (void)drawCircleWithOrigin:(CGPoint)origin inContext:(CGContextRef)context{
    UIColor *drawColor = [UIColor colorWithHexString:BLUE_COLOR_HEX];
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context,drawColor.CGColor);
    CGContextSetFillColorWithColor(context, drawColor.CGColor);
    CGRect theRect = CGRectMake(origin.x,origin.y,circleDiameter,circleDiameter);
    CGContextAddEllipseInRect(context, theRect);
    CGContextDrawPath(context, kCGPathFillStroke);
}

- (void)drawWeekDaysNamesInContext:(CGContextRef)context {
    NSArray *weekDaysStr = @[@"Пн", @"Вт", @"Ср", @"Чт", @"Пт", @"Сб", @"Вс"];
    
    UIFont *theFont;
    
    for ( int i = 0, x = datesStartX; i < [currentWeekArray count]; i++, x += circleLayout ) {
        theFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize];
        CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
        
        if ( [self selectedDateIndex] == i ) {
            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        } else if ( [[VATimeHandler today] compare:[currentWeekArray objectAtIndex:i]] == NSOrderedSame ) {
            CGContextSetFillColorWithColor(context, [UIColor colorWithHexString:BLUE_COLOR_HEX].CGColor);
            theFont = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
        } else if ( i == 5 || i == 6 ) {
            CGContextSetFillColorWithColor(context, [UIColor colorWithHexString:GREY_COLOR_HEX].CGColor);
        }
        [[weekDaysStr objectAtIndex: i] drawInRect:CGRectMake(x, circleY + 7, circleDiameter, circleDiameter) withFont:theFont lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentCenter];
    }
}

#pragma mark - Touches

- (int)indexOfTouchedDate:(CGPoint)touchLocation {
    for ( int i = 0, x = datesStartX; i < [currentWeekArray count]; i++, x += circleLayout ) {
        CGRect dateRect = CGRectMake(x, 0, viewHeight, circleDiameter);
        if ( CGRectContainsPoint(dateRect, touchLocation) ) {
            return i;
        }
    }
    return -1;
}

- (CGPoint)locationFromTouch:(UITouch *)touch {
    return [touch locationInView:[touch view]];
}

- (CGPoint)locationFromTouches:(NSSet *)touches {
    return [self locationFromTouch:[touches anyObject]];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint touchLocation = [self locationFromTouches:touches];
    int touchIndex = [self indexOfTouchedDate:touchLocation];
    if ( touchIndex > -1 ) {
        selectedDate = [currentWeekArray objectAtIndex:touchIndex];
        if ([delegate respondsToSelector:@selector(selectedDate:fromTouch:)]) {
            [delegate selectedDate:selectedDate fromTouch:YES];
        }
        [self animateCircles];
    }
}

#pragma mark - Animations

- (void)animateCircles {
    [self setNeedsDisplay];
    [UIView transitionWithView:self duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.layer displayIfNeeded];
                    } completion:nil];
}

@end
