//
//  UILabel+CustomLabel.m
//  IIR-Timetable
//
//  Created by Алексей on 28.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "UILabel+CustomLabel.h"

@implementation UILabel (CustomLabel)

+ (id)labelWithFrame:(CGRect)frame
            fontSize:(float)fontSize
           fontColor:(UIColor *)fontColor {
    UILabel *label = [self labelWithFontSize:fontSize fontColor:fontColor];
    label.frame = frame;
    
    return  label;
}

+ (id)labelWithFontSize:(float)fontSize fontColor:(UIColor *)fontColor {
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize];
    label.textColor = fontColor;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

@end
