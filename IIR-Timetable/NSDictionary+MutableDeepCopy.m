//
//  NSDictionary+MutableDeepCopy.m
//  IIR-Timetable
//
//  Created by Алексей on 06.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "NSDictionary+MutableDeepCopy.h"

@implementation NSDictionary (MutableDeepCopy)
- (NSMutableDictionary *)mutableDeepCopy {
    NSMutableDictionary *returnDict = [NSMutableDictionary dictionaryWithCapacity:[self count]];
    NSArray *keys = [self allKeys];
    for (id key in keys) {
        id oneValue = [self objectForKey:key];
        id oneCopy = nil;
        
        if ( [oneValue respondsToSelector:@selector(mutableDeepCopy)] ) {
            oneCopy = [oneValue mutableDeepCopy];
        } else if ( [oneValue respondsToSelector:@selector(mutableCopy)] ) {
            oneCopy = [oneValue mutableCopy];
        }
        if ( oneCopy == nil ) {
            oneCopy = [oneValue copy];
        }
        [returnDict setValue:oneCopy forKey:key];
    }
    return returnDict;
}
@end
