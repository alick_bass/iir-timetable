//
//  VAYoutubePlaylist.m
//  IIR-Timetable
//
//  Created by Алексей on 12.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAYoutubePlaylist.h"

@interface VAYoutubePlaylist ()

@end

static const NSString *youtubeUrl = @"playlistItems";
static const NSString *partValue = @"snippet";
static const NSString *itemsKey = @"items";

static const NSString *snippetKey = @"snippet";
static const NSString *resourceIdKey = @"resourceId";
static const NSString *videoIdKey = @"videoId";
static const NSString *titleKey = @"title";

static NSString *youtubeVideoFormat = @"<html><head><meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = %d\"/></head><body style=\"background:#000;margin-top:0px;margin-left:0px\"><div><object width=\"%d\" height=\"%d\"><param name=\"movie\" value=\"http://www.youtube.com/v/%@\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/%@\" type=\"application/x-shockwave-flash\" width=\"%d\" height=\"%d\" allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed></object></div></body></html>";

@implementation VAYoutubePlaylist

@synthesize playlistId = _playlistId;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(getPlaylistVideos) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self getPlaylistVideos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getPlaylistVideos {
    NSString *urlStr = [NSString stringWithFormat:@"%@?part=%@&playlistId=%@&maxResults=50&key=%@", youtubeUrl, partValue, _playlistId, YOUTUBE_API_KEY];
    Netwotk *network = [Netwotk networkWithBaseUrl:YOUTUBE_BASE_HREF
                                               url:urlStr
                                          delegate:self];
    [network startConnection];
}

#pragma mark - Network Delegate

- (void)netwotkFinishedLoadingData:(NSData *)data {
    NSError *error = nil;
    
    id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSAssert([result isKindOfClass:[NSDictionary class]], @"Not a dictionary");
    NSDictionary *resultDict = (NSDictionary *)result;
    NSArray *resultArray = [resultDict objectForKey:itemsKey];
    videos = resultArray;
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void)networkFailedToLoadData {
    [self.refreshControl endRefreshing];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [videos count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VideoCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *video = [videos objectAtIndex:indexPath.section];
    
    UIWebView *videoView = (UIWebView *)[cell viewWithTag:VideoCellTagWebView];
    videoView.scrollView.scrollEnabled = NO;
    
    [videoView loadHTMLString:@"<html><head></head><body></body></html>" baseURL:nil];
    
    int width = (int)videoView.frame.size.width;
    int height = (int)videoView.frame.size.height;
    
    NSString *videoId = [[[video objectForKey:snippetKey] objectForKey:resourceIdKey] objectForKey:videoIdKey];
    NSString *videoStr = [NSString stringWithFormat:youtubeVideoFormat, width, width, height, videoId, videoId, width, height];
    
    [videoView loadHTMLString:videoStr baseURL:nil];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:VideoCellTagTitle];
    titleLabel.text = [[video objectForKey:snippetKey] objectForKey:titleKey];
}

@end
