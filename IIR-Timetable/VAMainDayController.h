//
//  VAMainDayController.h
//  IIR-Timetable
//
//  Created by Alick Dikan on 21.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "VAWeekDaysView.h"
#import "VAAdvert.h"

@class VADayController;

@interface VAMainDayController : UIViewController <UIScrollViewDelegate, VAWeekDaysViewDelegate, VAAdvertDelegate> {
    UILabel *titleLabel;
    UILabel *learningWeek;
    VADayController *leftDayView;
    VADayController *selectedDayView;
    VADayController *rightDayView;
    VAWeekDaysView *weekDaysView;
    
    CGPoint tempContentOffset;
    
    NSLayoutConstraint *navBarOffsetConstraint;
    NSLayoutConstraint *navBarTopInsetConstraint;
}

@property (nonatomic, strong) UIView *navBar;
@property (nonatomic, strong) UIScrollView *mainScroll;
@property (nonatomic, strong) VAAdvert *adView;

@end
