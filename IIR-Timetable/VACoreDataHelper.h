//
//  VACoreDataHelper.h
//  IIR-Timetable
//
//  Created by Alick Dikan on 15.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface VACoreDataHelper : NSObject

+ (BOOL)entityHasData:(NSString *)entityName inContext:(NSManagedObjectContext *)context;
+ (void)deleteAllInstancesInEntity:(NSString *)entityName inContext:(NSManagedObjectContext *)context;
+ (void)insertIntoCoreDataValues:(NSArray *)values forEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context;
+ (void)saveContext;
+ (NSManagedObjectContext *)mainContext;

+ (NSArray *)getAllDataFromEntity:(NSString *)entityName inContext:(NSManagedObjectContext *)context;

@end
