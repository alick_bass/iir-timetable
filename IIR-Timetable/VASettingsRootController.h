//
//  VASettingsRootController.h
//  IIR-Timetable
//
//  Created by Алексей on 07.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VASettingsRootController : UITableViewController {
    NSMutableDictionary *controllers;
    NSArray *keys;
}

@end
