//
//  VAYoutubePlaylist.h
//  IIR-Timetable
//
//  Created by Алексей on 12.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    VideoCellTagWebView = 1,
    VideoCellTagTitle,
} VideoCellTags;

@interface VAYoutubePlaylist : UITableViewController <NetworkDelegate> {
    NSArray *videos;
}

@property (nonatomic, retain) NSString *playlistId;

@end
