//
//  UIImage+Cache.h
//  VseBileti
//
//  Created by Алексей Дикань on 06.09.13.
//  Copyright (c) 2013 Алексей Дикань. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Cache)

+ (UIImage *)cachedImageWithUrl:(NSString *)imageUrl relativeUrl:(NSString *)relativeUrl;

- (UIImage *)squareImage;

@end
