//
//  VAAdvert.m
//  IIR-Timetable
//
//  Created by Алексей on 14.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAAdvert.h"
#import "Reachability.h"

@implementation VAAdvert

@synthesize delegate;

NSString * const adTypePortraitPhone = @"{320,50}";
NSString * const adTypePortraitPad = @"{768,66}";

static const float adUpdateFrequency = 30.0f;

static const NSString *imgKey = @"img";
static const NSString *linkKey = @"link";
static const NSString *telKey = @"tel";

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        if ( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ) {
            _adType = adTypePortraitPad;
        } else {
            _adType = adTypePortraitPhone;
        }
        [self setupAdConnection];
    }
    return self;
}

+ (id)adWithOrigin:(CGPoint)origin {
    NSString *adType;
    
    if ( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ) {
        adType = adTypePortraitPad;
    } else {
        adType = adTypePortraitPhone;
    }
    
    CGSize adSize = CGSizeFromString(adType);
    
    CGRect frame = CGRectMake(origin.x, origin.y, adSize.width, adSize.height);
    return [[self alloc] initWithFrame:frame];
}

+ (id)ad {
    return [[self alloc] initWithFrame:CGRectZero];
}

- (void)setOrigin:(CGPoint)origin {
    CGRect newFrame = CGRectMake(origin.x, origin.y, self.frame.size.width, self.frame.size.height);
    self.frame = newFrame;
}

#pragma mark - VAAdConnection Methods

- (void)setupAdConnection {
    VAAdConnection *adConnection = [VAAdConnection adConnection];
    adConnection.delegate = self;
    [adConnection downloadAds];
    timer = [NSTimer scheduledTimerWithTimeInterval:adUpdateFrequency target:adConnection selector:@selector(downloadAds) userInfo:nil repeats:YES];
}

- (void)adConnectionRecievedAd:(NSDictionary *)ad {
    currentAd = ad;
    [self changeAdsImage:[ad objectForKey:imgKey]];
}

- (void)adConnectionFailedToReceiveAd {
    if ( [self.delegate respondsToSelector:@selector(advertFailedToLoadAd)] ) {
        [self.delegate advertFailedToLoadAd];
    }
}

#pragma mark - 

- (void)changeAdsImage:(NSString *)imgUrlStr {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        UIImage *adImage = [UIImage cachedImageWithUrl:imgUrlStr relativeUrl:nil];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ( !adImage ) {
                if ( [self.delegate respondsToSelector:@selector(advertFailedToLoadAd)] )
                    [self.delegate advertFailedToLoadAd];
            } else {
                self.image = adImage;
                if ( [self.delegate respondsToSelector:@selector(advertLoadedAd)] )
                    [self.delegate advertLoadedAd];
            }
        });
    });
}

#pragma mark - Touch

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    Reachability *wifiReachibility = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [wifiReachibility currentReachabilityStatus];
    
    NSString *urlStr;
    if ( networkStatus == NotReachable ) {
        urlStr = [NSString stringWithFormat:@"tel:%@", [currentAd objectForKey:telKey]];
    } else {
        urlStr = [currentAd objectForKey:linkKey];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}

@end