//
//  VADevelopersController.h
//  IIR-Timetable
//
//  Created by Алексей on 09.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    DevelopesCellTagTextView = 1,
} DevelopesCellTags;

@interface VADevelopersController : UITableViewController {
    NSArray *developesInfo;
}

@end
