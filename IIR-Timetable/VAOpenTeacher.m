//
//  VAOpenTeacher.m
//  IIR-Timetable
//
//  Created by Алексей on 06.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAOpenTeacher.h"
#import "VADataBaseController.h"
#import "VATimeHandler.h"

@interface VAOpenTeacher ()

@end

@implementation VAOpenTeacher

const static NSString *timeKey  = @"time";
const static NSString *weekKey  = @"week";
const static NSString *typeKey  = @"type";
const static NSString *titleKey = @"title";
const static NSString *roomKey  = @"room";

@synthesize teacherName = _teacherName;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = _teacherName;
    
    [self prepareTeachersTimeTable];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper

- (NSString *)typeStrFromType:(LessonTypes)type {
    NSString *typeStr;
    switch (type) {
        case LessonTypeSeminar:
            typeStr = @"Семінар";
            break;
            
        case LessonTypeLection:
            typeStr = @"Лекція";
            break;
            
        default:
            typeStr = nil;
            break;
    }
    return typeStr;
}

#pragma mark - Data Base Methods

- (void)prepareTeachersTimeTable {
    VADataBaseController *dataBaseController = [VADataBaseController timeTableBase];
    NSDictionary *subjects = [dataBaseController openTeacher:_teacherName];
    teachersTimeTable = subjects;
    keys = [[subjects allKeys] sortedArrayUsingSelector:@selector(compare:)];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [keys count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [keys objectAtIndex:section];
    return [[teachersTimeTable objectForKey:key] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OpenTeacherCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *key = [keys objectAtIndex:indexPath.section];
    NSDictionary *subject = [[teachersTimeTable objectForKey:key] objectAtIndex:indexPath.row];
    
    
    UILabel *timeLabel = (UILabel *)[cell viewWithTag:TeacherCellTagTime];
    NSMutableString *timeStr = [NSMutableString stringWithFormat:@"%@ пара", [subject objectForKey:timeKey]];
    NSString *weekStr = [VATimeHandler learningWeekStrFromWeek:[[subject objectForKey:weekKey] intValue]];
    if ( weekStr != nil ) {
        [timeStr appendFormat:@", %@", weekStr];
    }
    timeLabel.text = timeStr;
    
    UILabel *typeLabel = (UILabel *)[cell viewWithTag:TeacherCellTagType];
    typeLabel.text = [self typeStrFromType:[[subject objectForKey:typeKey] intValue]];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:TeacherCellTagTitle];
    titleLabel.text = [subject objectForKey:titleKey];
    
    UILabel *roomLabel = (UILabel *)[cell viewWithTag:TeacherCellTagRoom];
    roomLabel.text = [subject objectForKey:roomKey];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *key = [keys objectAtIndex:section];
    return [VATimeHandler weekDayName:[key intValue]];
}

@end
