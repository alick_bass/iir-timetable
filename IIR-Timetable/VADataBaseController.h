//
//  VADataBaseController.h
//  IIR-Timetable
//
//  Created by Алексей on 21.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

#define CORE_DATA_ENTITY_NAME   @"Timetable"
#define CORE_DATA_EXAMS_NAME    @"Exams"
#define CORE_DATA_CREDITS_NAME  @"Credits"
#define TIMETABLE_TIME_KEY      @"time"
#define LEARNING_WEEK_KEY       @"learningWeek"

@interface VADataBaseController : NSObject {
    sqlite3 *db;
}

@property (nonatomic, strong) NSManagedObjectContext *managedContext;

+ (id)timeTableBase;
- (void)fillInCacheDataBase;
- (NSDictionary *)getTimetableForWeekday:(WeekDay)weekDay
                                    week:(LearningWeek)learningWeek;
- (NSArray *)getExams;
- (NSArray *)getCredits;

- (NSArray *)teachers;
- (NSDictionary *)openTeacher:(NSString *)teacherName;

- (int)getLearningWeek;

- (NSArray *)settingsValueForColumn:(NSString *)column;

@end
