//
//  VAAdConnection.m
//  IIR-Timetable
//
//  Created by Alick Dikan on 15.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAAdConnection.h"
#import "VACoreDataHelper.h"
#import "Reachability.h"

@implementation VAAdConnection

static NSString *adEntityName = @"VAAdvert";
static NSString *urlStr = @"http://avtokursi.com/iOS/ad.json";

static const NSString *priorityKey = @"priority";
static const NSString *lowLimitKey = @"lowLimit";
static const NSString *highLimitKey = @"highLimit";

static const int maxPriority = 100;

@synthesize context = _context;
@synthesize delegate;

- (id)init {
    self = [super init];
    if ( self ) {
        _context = [VACoreDataHelper mainContext];
    }
    return self;
}

+ (id)adConnection {
    return [[self alloc] init];
}

- (void)downloadAds {
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    Reachability *wifiReachibility = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [wifiReachibility currentReachabilityStatus];
    
    if ( networkStatus == NotReachable ) {
        [self failureHanling];
    } else {
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            if ( error ) {
                [self failureHanling];
            } else {
                [self handleRecievedData:data];
            }
        }];
    }
}

#pragma mark - Download Completion Handling

- (void)handleRecievedData:(NSData *)data {
    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSAssert([jsonData isKindOfClass:[NSArray class]], @"JSON returned not an array");
    
    NSArray *ads = (NSArray *)jsonData;
    [VACoreDataHelper deleteAllInstancesInEntity:adEntityName inContext:_context];
    [VACoreDataHelper insertIntoCoreDataValues:ads forEntityName:adEntityName inContext:_context];
    [self handlePriorityWithAds:ads];
}

- (void)failureHanling {
    if ( [VACoreDataHelper entityHasData:adEntityName inContext:_context] == NO ) {
        if ( [self.delegate respondsToSelector:@selector(adConnectionFailedToReceiveAd)] ) {
            [self.delegate adConnectionFailedToReceiveAd];
        }
        return;
    }
    NSArray *ads = [VACoreDataHelper getAllDataFromEntity:adEntityName inContext:_context];
    [self handlePriorityWithAds:ads];
}

#pragma marl - Priority Handling

- (void)handlePriorityWithAds:(NSArray *)ads {
    int randomIndex = arc4random_uniform(maxPriority);
    int currentLimit = 0;
    
    for ( NSMutableDictionary *ad in ads ) {
        int lowLimit;
        int highLimit;
        int adPriority = [[ad objectForKey:priorityKey] intValue];
        
        lowLimit = currentLimit;
        currentLimit += adPriority;
        highLimit = currentLimit;
        
        if ( randomIndex >= lowLimit && randomIndex < highLimit ) {
            [self showAd:ad];
        }
    }
}

- (void)showAd:(NSDictionary *)ad {
    if ( [self.delegate respondsToSelector:@selector(adConnectionRecievedAd:)] )
        [self.delegate adConnectionRecievedAd:ad];
}

@end
