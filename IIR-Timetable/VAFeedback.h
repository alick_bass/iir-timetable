//
//  VAFeedback.h
//  IIR-Timetable
//
//  Created by Алексей on 13.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface VAFeedback : UITableViewController <UITextViewDelegate, NetworkDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITextView *messageView;

- (IBAction)sendFeedback:(id)sender;

@end
