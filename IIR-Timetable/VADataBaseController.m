//
//  VADataBaseController.m
//  IIR-Timetable
//
//  Created by Алексей on 21.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VADataBaseController.h"
#import "VAAppDelegate.h"
#import "VAUser.h"
#import "VACoreDataHelper.h"

@implementation VADataBaseController

@synthesize managedContext = _context;

#pragma mark - LifeCycle

+ (id)timeTableBase {
    return [[self alloc] init];
}

- (id)init {
    self = [super init];
    
    if ( self ) {
        _context = [VACoreDataHelper mainContext];
    }
    return self;
}

- (void)fillInCacheDataBase {
    [self openTimetableDataBase];
    [VACoreDataHelper deleteAllInstancesInEntity:CORE_DATA_ENTITY_NAME inContext:_context];
    [VACoreDataHelper deleteAllInstancesInEntity:CORE_DATA_EXAMS_NAME inContext:_context];
    [VACoreDataHelper deleteAllInstancesInEntity:CORE_DATA_CREDITS_NAME inContext:_context];
    
    VAUser *user = [VAUser currentUser];
    [user checkSettings];
    NSArray *timetable = [self getFullTimetableForUser:user];
    [VACoreDataHelper insertIntoCoreDataValues:timetable forEntityName:CORE_DATA_ENTITY_NAME inContext:_context];
    
    NSArray *exams = [self examsFromDatabaseForUser:user];
    [VACoreDataHelper insertIntoCoreDataValues:exams forEntityName:CORE_DATA_EXAMS_NAME inContext:_context];
    
    NSArray *credits = [self creditsFromDatabaseForUser:user];
    [VACoreDataHelper insertIntoCoreDataValues:credits forEntityName:CORE_DATA_CREDITS_NAME inContext:_context];
    
    [self updateCurrentLearningWeek];
    [self closeDB];
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_TIMETABLE_NOTIFICATION object:self];
}

- (NSArray *)teachers {
    [self openTimetableDataBase];
    NSArray *teachers = [self teachersFromTimeTable];
    [self closeDB];
    return teachers;
}

- (NSDictionary *)openTeacher:(NSString *)teacherName {
    [self openTimetableDataBase];
    NSArray *subjects = [self teachersTimeTable:teacherName];
    [self closeDB];
    
    NSMutableDictionary *subjectsDict = [NSMutableDictionary dictionary];
    for ( NSDictionary *subject in subjects ) {
        NSString *weekday = [subject objectForKey:@"weekday"];
        NSMutableArray *array = [subjectsDict objectForKey:weekday];
        if ( !array ) {
            array = [NSMutableArray array];
            [subjectsDict setObject:array forKey:weekday];
        }
        [array addObject:subject];
    }
    
    return subjectsDict;
}

- (NSArray *)settingsValueForColumn:(NSString *)column {
    [self openTimetableDataBase];
    NSArray *result = [self settingsForSettingsName:column];
    [self closeDB];
    return result;
}

#pragma mark - SQL Methods

- (NSString *)pathInDocumentsDirectoryToFileNamed:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:fileName];
}

-(NSString *)pathInMainBundleToFileNamed:(NSString *)fileName
                                  ofType:(NSString *)fileType {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *filePath = [bundle pathForResource:fileName ofType:fileType];
    return filePath;
}

- (void)openDataBaseWithPath:(NSString *)pathToFile {
    if (sqlite3_open([pathToFile UTF8String], &db) != SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(0, @"DataBase failed to open");
    }
}

- (void)openTimetableDataBase {
    NSString *dbName = [NSString stringWithFormat:@"%@.sqlite3", DATABASE_NAME];
    NSString *pathForDB = [self pathInDocumentsDirectoryToFileNamed:dbName];
    NSFileManager *defaultFileManager = [NSFileManager defaultManager];
    
    if ( [defaultFileManager fileExistsAtPath:pathForDB] ) {
        [self openDataBaseWithPath:pathForDB];
    } else {
        NSString *pathToDBInMainBundle = [self pathInMainBundleToFileNamed:DATABASE_NAME ofType:@"sqlite3"];
        NSError *error;
        [defaultFileManager copyItemAtPath:pathToDBInMainBundle toPath:pathForDB error:&error];
        
        if ( error == nil ) {
            [self openDataBaseWithPath:pathForDB];
        }
    }
}

- (NSArray *)getFullTimetableForUser:(VAUser *)user andNeededColumns:(NSArray *)keys table:(NSString *)tableName {
    NSMutableArray *array = [NSMutableArray array];
    
    NSMutableString *query = [NSMutableString stringWithFormat:@"SELECT `%@`", [keys objectAtIndex:0]];
    
    for ( int i = 1; i < [keys count]; i++ ) {
        [query appendFormat:@", `%@`", [keys objectAtIndex:i]];
    }
    
    NSArray *userValues = [VAUser userValues];
    
    NSDictionary *userSetting = [userValues firstObject];
    if ( [(NSNumber *)[userSetting objectForKey:USER_ISOPTIONAL_KEY] boolValue] == NO ) {
        NSString *title = [userSetting objectForKey:USER_TITLE_KEY];
        [query appendFormat:@" FROM `%@` WHERE (`%@` IS %d)", tableName, title, [user getValueForKey:title]];
    } else {
        NSString *title = [userSetting objectForKey:USER_TITLE_KEY];
        [query appendFormat:@" FROM `%@` WHERE (`%@` IS %d OR `%@` ISNULL)", tableName, title, [user getValueForKey:title], title];
    }
    
    for ( int i = 1; i < [userValues count]; i++ ) {
        NSDictionary *setting = [userValues objectAtIndex:i];
        
        if ( [(NSNumber *)[setting objectForKey:USER_ISOPTIONAL_KEY] boolValue] == NO ) {
            NSString *title = [setting objectForKey:USER_TITLE_KEY];
            [query appendFormat:@" AND (`%@` IS %d)", title, [user getValueForKey:title]];
        } else {
            NSString *title = [setting objectForKey:USER_TITLE_KEY];
            [query appendFormat:@" AND (`%@` IS %d OR `%@` ISNULL )", title, [user getValueForKey:title], title];
        }
    }
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(db, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:[keys count]];
            for ( int i=0 ; i < [keys count]; i++ ) {
                char *field = (char *)sqlite3_column_text(statement, i);
                NSString *fieldStr;
                if ( field == nil ) {
                    fieldStr = @"";
                } else {
                    fieldStr = [NSString stringWithUTF8String:field];
                }
                [dict setObject:fieldStr forKey:[keys objectAtIndex:i]];
            }
            [array addObject:dict];
        }
    } else {
        NSLog(@"Failed to select from database");
    }
    sqlite3_finalize(statement);
    return array;
}

- (NSArray *)getFullTimetableForUser:(VAUser *)user {
    NSArray *keys = @[@"id", @"week", @"weekday", @"time", @"type", @"room", @"teacher", @"title"];
    return [self getFullTimetableForUser:user andNeededColumns:keys table:@"timeTable"];
}

- (int)getTimetableLearningWeek {
    int learningWeek = 0;
    NSString *query = [NSString stringWithFormat:@"SELECT `highWeek` FROM `settings` LIMIT 0,1"];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(db, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            learningWeek = (int)sqlite3_column_int(statement, 0);
        }
    } else {
        NSLog(@"Failed getting learning Week");
    }
    sqlite3_finalize(statement);
    return learningWeek;
}

- (NSArray *)teachersFromTimeTable {
    NSMutableArray *teachers = [NSMutableArray array];
    
    NSString *query = [NSString stringWithFormat:@"SELECT DISTINCT `teacher` FROM `timeTable`"];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(db, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *teacherName = (char *)sqlite3_column_text(statement, 0);
            [teachers addObject:[NSString stringWithUTF8String:teacherName]];
        }
    } else {
        NSLog(@"Failed getting all teachers");
    }
    sqlite3_finalize(statement);
    return teachers;
}

- (NSArray *)teachersTimeTable:(NSString *)teacherName {
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *keys = @[@"weekday", @"time", @"week", @"type", @"room", @"title"];
    
    NSMutableString *query = [NSMutableString stringWithFormat:@"SELECT `%@`", [keys objectAtIndex:0]];
    for ( int i = 1; i < [keys count]; i++ ) {
        [query appendFormat:@", `%@`", [keys objectAtIndex:i]];
    }
    [query appendFormat:@"from `timeTable` WHERE `teacher` IS '%@' ORDER BY `%@` ASC", teacherName, [keys firstObject]];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(db, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:[keys count]];
            for ( int i=0 ; i < [keys count]; i++ ) {
                char *field = (char *)sqlite3_column_text(statement, i);
                NSString *fieldStr = [NSString stringWithUTF8String:field];
                [dict setObject:fieldStr forKey:[keys objectAtIndex:i]];
            }
            [array addObject:dict];
        }
    } else {
        NSLog(@"Failed to load openTeacher data");
    }
    sqlite3_finalize(statement);
    
    NSArray *resultArray = [array sortedArrayUsingComparator:^(id a, id b) {
        NSDictionary *aDict = (NSDictionary *)a;
        NSDictionary *bDict = (NSDictionary *)b;
        
        for ( int i = 0; i < 3; i++ ) {
            NSString *value1 = [aDict objectForKey:[keys objectAtIndex:i]];
            NSString *value2 = [bDict objectForKey:[keys objectAtIndex:i]];
            if ( [value1 caseInsensitiveCompare:value2] == NSOrderedAscending ) {
                return NSOrderedAscending;
            }
        }
        return NSOrderedDescending;
    }];
    return resultArray;
}

- (NSArray *)settingsForSettingsName:(NSString *)columnName {
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *userValues = [VAUser userValues];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF[%@] == %@", USER_TITLE_KEY, columnName];
    userValues = [userValues filteredArrayUsingPredicate:filter];
    if ( userValues == nil ) {
        return nil;
    }
    
    NSDictionary *value = [userValues firstObject];
    
    id table = [value objectForKey:USER_TABLE_KEY];
    
    NSMutableString *query = [NSMutableString string];
    
    if ( table == [NSNull null] ) {
        [query appendFormat:@"SELECT DISTINCT `%@`, `%@` AS `name` FROM `timeTable`", columnName, columnName];
    } else {
        NSString *tableStr = (NSString *)table;
        [query appendFormat:@"SELECT DISTINCT `%@`, `%@`.`name` FROM `timeTable` INNER JOIN `%@` ON `%@`=`%@`.`id`", columnName, tableStr, tableStr, columnName, tableStr];
    }
    
    [query appendFormat:@" WHERE (`%@` NOTNULL)", columnName];
    
    BOOL isOptional = [[value objectForKey:USER_ISOPTIONAL_KEY] boolValue];
    
    if ( isOptional ) {
        VAUser *user = [VAUser currentUser];
        [user checkSettings];
        NSArray *requiredFields = [VAUser userRequiredValues];
        
        for ( NSDictionary *requiredField in requiredFields ) {
            NSString *requiredTitle = [requiredField objectForKey:USER_TITLE_KEY];
            int requiredValue = [user getValueForKey:requiredTitle];
            [query appendFormat:@" AND (`%@` IS %d)", requiredTitle, requiredValue];
        }
    }
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(db, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:2];
            
            char *titleField = (char *)sqlite3_column_text(statement, 0);
            [dict setObject:[NSString stringWithUTF8String:titleField] forKey:columnName];
            
            char *nameField = (char *)sqlite3_column_text(statement, 1);
            [dict setObject:[NSString stringWithUTF8String:nameField] forKey:TABLE_SETTING_NAME_KEY];
            
            [array addObject:dict];
        }
    }
    
    return array;
}

- (NSArray *)examsFromDatabaseForUser:(VAUser *)user {
    NSArray *keys = @[@"consDate", @"consRoom", @"examDate", @"examRoom", @"teacher", @"title"];
    return [self getFullTimetableForUser:user andNeededColumns:keys table:@"exams"];
}

- (NSArray *)creditsFromDatabaseForUser:(VAUser *)user {
    NSArray *keys = @[@"date", @"room", @"teacher", @"title"];
    return [self getFullTimetableForUser:user andNeededColumns:keys table:@"credits"];
}

- (void)closeDB {
    sqlite3_close(db);
}

#pragma mark - CoreData Stack

- (NSDictionary *)getTimetableForWeekday:(WeekDay)weekDay
                                    week:(LearningWeek)learningWeek {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:CORE_DATA_ENTITY_NAME
                                              inManagedObjectContext:_context];
    [request setEntity:entity];
    
    NSPredicate *predicateWeek = [NSPredicate predicateWithFormat:@"week=%d OR week=0", learningWeek];
    NSPredicate *predicateWeekday = [NSPredicate predicateWithFormat:@"weekday=%d", weekDay];
    NSArray *subPredicates = @[predicateWeek, predicateWeekday];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [request setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:TIMETABLE_TIME_KEY
                                                                ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *results = [_context executeFetchRequest:request error:&error];
    
    NSDictionary *attributes = [entity attributesByName];
    
    if ( results == nil ) {
        NSLog(@"Unresolved error");
        return nil;
    } else {
        NSMutableDictionary *resultsDict = [NSMutableDictionary dictionaryWithCapacity:[results count]];
        for ( NSManagedObject *managedObject in results ) {
            
            NSMutableDictionary *subject = [NSMutableDictionary dictionaryWithCapacity:[attributes count]];
            
            for ( NSString *attributeKey in attributes ) {
                [subject setObject:[managedObject valueForKey:attributeKey] forKey:attributeKey];
            }
            
            [resultsDict setObject:subject forKey:[managedObject valueForKey:TIMETABLE_TIME_KEY]];
        }
        return resultsDict;
    }
}

- (NSArray *)getExams {
    return [VACoreDataHelper getAllDataFromEntity:CORE_DATA_EXAMS_NAME inContext:_context];
}

- (NSArray *)getCredits {
    return [VACoreDataHelper getAllDataFromEntity:CORE_DATA_CREDITS_NAME inContext:_context];
}

#pragma mark - User Defaults Methods

- (int)getLearningWeek {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return (int)[defaults integerForKey:LEARNING_WEEK_KEY];
}

- (void)updateCurrentLearningWeek {
    int week = [self getTimetableLearningWeek];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:week forKey:LEARNING_WEEK_KEY];
    [defaults synchronize];
}

@end
