//
//  UIImage+Cache.m
//  VseBileti
//
//  Created by Алексей Дикань on 06.09.13.
//  Copyright (c) 2013 Алексей Дикань. All rights reserved.
//

#import "UIImage+Cache.h"
#import "Reachability.h"

@implementation UIImage (Cache)

+(UIImage *)cachedImageWithUrl:(NSString *)imageUrlStr relativeUrl:(NSString *)relativeUrl {
    NSString *cachedImageName = [imageUrlStr stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    cachedImageName = [NSString stringWithFormat:@"/%@", cachedImageName];
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSFileManager *cachesFileManager = [NSFileManager defaultManager];
    
    cachesDirectory = [NSString stringWithFormat:@"%@/img", cachesDirectory];
    NSError *error;
    if (![cachesFileManager fileExistsAtPath:cachesDirectory])
        [cachesFileManager createDirectoryAtPath:cachesDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    
    NSString *cachedImagePath = [cachesDirectory stringByAppendingString:cachedImageName];
    BOOL imageExistsInFileDirectory = [cachesFileManager fileExistsAtPath:cachedImagePath];
    
    Reachability *wifiReachibility = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [wifiReachibility currentReachabilityStatus];
    
    if ( !imageExistsInFileDirectory && networkStatus == NotReachable ) {
        return nil;
    }
    
    if ( imageExistsInFileDirectory == YES && networkStatus != ReachableViaWiFi ) {
        return [UIImage imageWithContentsOfFile:cachedImagePath];
    } else {
        NSURL *imageURL = [NSURL URLWithString:imageUrlStr relativeToURL:[NSURL URLWithString:relativeUrl]];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        [imageData writeToFile:cachedImagePath atomically:YES];
        
        return [UIImage imageWithData:imageData];
    }
}

- (UIImage *)squareImage {
    float width = self.size.width;
    float height = self.size.height;
    
    float imgDiameter = MIN(width, height);
    
    CGRect cropRect = CGRectMake((width - imgDiameter) / 2, (height - imgDiameter) / 2, imgDiameter, imgDiameter);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return croppedImage;
}

@end
