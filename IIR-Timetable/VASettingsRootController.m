//
//  VASettingsRootController.m
//  IIR-Timetable
//
//  Created by Алексей on 07.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VASettingsRootController.h"
#import "VAUser.h"
#import "VAUserSettingsController.h"

@interface VASettingsRootController ()

@end

static NSString *settingsIdentifier = @"SettingsSegue";

const static NSString *segueIdKey = @"segueId";
const static NSString *nameKey = @"name";

@implementation VASettingsRootController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    controllers = [NSMutableDictionary dictionary];
    [self addSettingsSection];
    keys = [controllers allKeys];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controllers Preparations

- (void)addSettingsSection {
    NSString *key = @"Основні налаштування";
    
    NSArray *userSettings = [VAUser userValues];
    
    for ( NSMutableDictionary *dict in userSettings ) {
        [dict setObject:settingsIdentifier forKey:segueIdKey];
    }
    
    [controllers setObject:userSettings forKey:key];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [keys count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [keys objectAtIndex:section];
    return [[controllers objectForKey:key] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SettingsRootCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [keys objectAtIndex:indexPath.section];
    NSDictionary *controller = [[controllers objectForKey:key] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [controller objectForKey:nameKey];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [keys objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [keys objectAtIndex:indexPath.section];
    NSDictionary *controller = [[controllers objectForKey:key] objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:[controller objectForKey:segueIdKey] sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
    NSString *key = [keys objectAtIndex:selectedRowIndex.section];
    NSDictionary *controller = [[controllers objectForKey:key] objectAtIndex:selectedRowIndex.row];
    
    
    if ( [segue.identifier isEqualToString:settingsIdentifier] ) {
        VAUserSettingsController *destController = [segue destinationViewController];
        destController.settingsTitle = [controller objectForKey:USER_TITLE_KEY];
        destController.title = [controller objectForKey:nameKey];
    }
}


@end
