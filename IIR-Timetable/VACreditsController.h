//
//  VACreditsController.h
//  IIR-Timetable
//
//  Created by Алексей on 08.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CreditsCellTagTitle = 1,
    CreditsCellTagTeacher,
    CreditsCellTagDate,
    CreditsCellTagRoom,
} CreditsCellTags;

@interface VACreditsController : UITableViewController {
    NSArray *credits;
}

@end
