//
//  NSDictionary+MutableDeepCopy.h
//  IIR-Timetable
//
//  Created by Алексей on 06.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MutableDeepCopy)
- (NSMutableDictionary *)mutableDeepCopy;
@end
