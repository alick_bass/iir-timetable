//
//  VADefaults.h
//  IIR-Timetable
//
//  Created by Алексей on 21.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#ifndef IIR_TIMETABLE
#define IIR_TIMETABLE

#define DATABASE_NAME   @"Timetable"
#define BLUE_COLOR_HEX  @"0x007df6"
#define GREY_COLOR_HEX  @"0x9f9f9f"
#define LIGHT_BLUE_COLOR_HEX    @"0x81befa"

#define SYSTEM_VERSION_EQUAL_TO(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

#define SYSTEM_VERSION_GREATER_THAN(v)          ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define STATUS_BAR_HEIGHT   (SYSTEM_VERSION_LESS_THAN(@"7.0") ? 0.0 : 20.0)
#define TABBAR_HEIGHT       (SYSTEM_VERSION_LESS_THAN(@"7.0") ? 0.0 : 49.0)


#define USER_TITLE_KEY      @"title"
#define USER_NAME_KEY       @"name"
#define USER_ISOPTIONAL_KEY @"isOptional"
#define USER_TABLE_KEY      @"table"

#define TABLE_SETTING_NAME_KEY  @"name"

#define LESSON_ONE_START    @"08:00"
#define LESSON_ONE_END      @"09:20"
#define LESSON_TWO_START    @"09:30"
#define LESSON_TWO_END      @"10:50"
#define LESSON_THREE_START  @"11:10"
#define LESSON_THREE_END    @"12:30"
#define LESSON_FOUR_START   @"12:40"
#define LESSON_FOUR_END     @"14:00"

#define UPDATE_TIMETABLE_NOTIFICATION @"updateTimetable"


#define CANCEL_NETWOTK_NOTIFICATION_NAME @"cancelNetwork"
#define MAIN_URL @"http://iir-timetable"

#define YOUTUBE_BASE_HREF @"https://www.googleapis.com/youtube/v3/"
#define YOUTUBE_API_KEY @"AIzaSyDZI9eLeJI7GhpVpOYCxXbJvO4OgBu5N7o"
#define YOUTUBE_CHANNELID   @"UC_OT1RT9Fx0DbO4JbgvhFhQ"

#define NAVBAR_HEXCOLOR @"0xf8f8f8"
#define NAVBAR_LINE_COLOR   @"0xa7a7aa"

typedef enum {
    LearningWeekHigh = 1,
    LearningWeekLow,
} LearningWeek;

typedef enum {
    Sunday = 1,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
} WeekDay;

typedef enum {
    LessonTypeLection = 1,
    LessonTypeSeminar,
}LessonTypes;

typedef enum {
    TabBarToday,
    TabBarInfo,
    TabBarYoutube,
    TabBarSettings,
} TabBarTabs;

#endif
