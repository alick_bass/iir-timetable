//
//  VAOpenTeacher.h
//  IIR-Timetable
//
//  Created by Алексей on 06.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TeacherCellTagTime = 1,
    TeacherCellTagType,
    TeacherCellTagTitle,
    TeacherCellTagRoom,
}TeacherCellTag;

@interface VAOpenTeacher : UITableViewController {
    NSDictionary *teachersTimeTable;
    NSArray *keys;
}

@property (nonatomic, retain) NSString *teacherName;

@end
