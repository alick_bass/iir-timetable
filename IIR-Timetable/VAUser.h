//
//  VAUser.h
//  IIR-Timetable
//
//  Created by Алексей on 22.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VAUser : NSObject

+ (id)currentUser;
+ (NSArray *)userValues;
+ (NSArray *)userRequiredValues;
- (int)getValueForKey:(NSString *)key;
- (void)update:(int)value forKey:(NSString *)key;
- (void)checkSettings;

@end
