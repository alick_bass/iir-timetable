//
//  VAExamsController.m
//  IIR-Timetable
//
//  Created by Алексей on 08.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "VAExamsController.h"
#import "VADataBaseController.h"
#import "VATimeHandler.h"

@interface VAExamsController ()

@end

const static NSString *titleKey = @"title";
const static NSString *teacherKey = @"teacher";
const static NSString *consDateKey = @"consDate";
const static NSString *consRoomKey = @"consRoom";
const static NSString *examDateKey = @"examDate";
const static NSString *examRoomKey = @"examRoom";

@implementation VAExamsController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareExams];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareExams) name:UPDATE_TIMETABLE_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareExams {
    exams = nil;
    VADataBaseController *databaseController = [VADataBaseController timeTableBase];
    NSArray *array = [databaseController getExams];
    array = [array sortedArrayUsingComparator:^(id a, id b){
        NSDictionary *dictA = (NSDictionary *)a;
        NSDictionary *dictB = (NSDictionary *)b;
        
        NSString *stringA = [dictA objectForKey:examDateKey];
        NSString *stringB = [dictB objectForKey:examDateKey];
        
        NSDate *dateA = [VATimeHandler sessionDateFromString:stringA];
        NSDate *dateB = [VATimeHandler sessionDateFromString:stringB];
        
        return [dateA compare:dateB];
    }];
    exams = array;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"%d екзамен", (int)section + 1];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [exams count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ExamsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *exam = [exams objectAtIndex:indexPath.section];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:ExamCellTagTitle];
    titleLabel.text = [exam objectForKey:titleKey];
    
    UILabel *teacherLabel = (UILabel *)[cell viewWithTag:ExamCellTagTeacher];
    teacherLabel.text = [exam objectForKey:teacherKey];
    
    UILabel *consDateLabel = (UILabel *)[cell viewWithTag:ExamCellTagConsultDate];
    consDateLabel.text = [exam objectForKey:consDateKey];
    
    UILabel *consRoomLabel = (UILabel *)[cell viewWithTag:ExamCellTagConsultRoom];
    consRoomLabel.text = [exam objectForKey:consRoomKey];
    
    UILabel *examDateLabel = (UILabel *)[cell viewWithTag:ExamCellTagExamDate];
    examDateLabel.text = [exam objectForKey:examDateKey];
    
    UILabel *examRoomLabel = (UILabel *)[cell viewWithTag:ExamCellTagExamRoom];
    examRoomLabel.text = [exam objectForKey:examRoomKey];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
