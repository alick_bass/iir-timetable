//
//  VADayController.h
//  IIR-Timetable
//
//  Created by Алексей on 30.10.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum {
    CellTagTypeLabel = 1,
    CellTagTitleLabel,
    CellTagTecherLabel,
    CellTagRoomLabel,
    CellTagLeftTimeLabel,
    CellTagLeftTimeBackground,
}CellTags;

@interface VADayController : UITableViewController {
    NSDictionary *lessonsDict;
    NSTimer *timer;
}

@property (nonatomic, strong) NSDate *neededDate;

+ (id)dayControllerWithDate:(NSDate *)date;
- (void)resetToDate:(NSDate *)date;

@end
