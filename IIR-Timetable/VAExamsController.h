//
//  VAExamsController.h
//  IIR-Timetable
//
//  Created by Алексей on 08.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ExamCellTagTitle = 1,
    ExamCellTagTeacher,
    ExamCellTagConsultDate,
    ExamCellTagConsultRoom,
    ExamCellTagExamDate,
    ExamCellTagExamRoom,
}ExamCellTags;

@interface VAExamsController : UITableViewController {
    NSArray *exams;
}

@end
